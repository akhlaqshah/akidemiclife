import 'dart:io';
import 'package:akidemic_life/utils/size_config.dart';

SizeConfig sizeConfig;
File newImage;

List<String> eventsList = [
  'Childcare', 'Counselling', 'Events', 'Food', 'Fitness', 'Healthcare', 'Legal Advice', 'Meetups', 'Outdoors', 'Playgrounds', 'Playgroups', 'Other'
];