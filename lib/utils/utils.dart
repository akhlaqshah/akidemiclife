import 'package:akidemic_life/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {

  static bool alreadyLoggedIn = false;

  static launchURL(String url) async {
    //const url = 'https://flutter.io';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  static addValueToSF(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static makeLoginStatus(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(Constants.loginStatus, value);
  }

  static makeLogoutStatus(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(Constants.loginStatus, value);
  }

  static rememberUser(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(Constants.rememberUser, value);
  }


  static Future<bool> getRememberMeStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool(Constants.rememberUser);
    return boolValue;
  }


  static Future<bool> getLoginStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool(Constants.loginStatus);
    return boolValue;
  }


  /// save user info in shared preferences

  static saveFullNameInSP(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.fullName, value);
  }

  static savePhoneNoInSP(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.phoneNo, value);
  }

  static saveInstituteNameInSP(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.instituteName, value);
  }

  static saveLocationInSP(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.location, value);
  }

  static saveSubscribeInSP(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.subscription, value);
  }

  static saveUserIdInSP(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.uid, value);
  }

  static saveEmailInSP(String uid, String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(uid, email);
  }









  /// get user info from shared preferences

  static Future<void> saveUIDToSF(String uidKey, String uidValue) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(uidKey, uidValue);

  }

  static Future<String> getUIDFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String uid = prefs.getString('uid');

    return uid;
  }

  /*static Future<String> getUIDFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String uid;

    if( prefs.containsKey(Constants.uid) ) {

      /// uid key exists in shared preferences
      uid = prefs.getString(Constants.uid);
    }
    else {
      /// uid key does not exists
      prefs.setString(Constants.uid, '');
      uid = prefs.getString(Constants.uid);
    }

    return uid;
  }*/


  static Future<String> getFullNameFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String name = prefs.getString(Constants.fullName);
    return name;
  }

  static Future<String> getPhoneNoFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String phoneNo = prefs.getString(Constants.phoneNo);
    return phoneNo;
  }

  static Future<String> getEmailFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String email = prefs.getString(Constants.email);
    return email;
  }

  static Future<String> getInstitutionNameFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String instituteName = prefs.getString(Constants.instituteName);
    return instituteName;
  }

  static Future<String> getLocationFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String location = prefs.getString(Constants.location);
    return location;
  }

  static Future<String> getSubscribtionFromSF() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String subscription = prefs.getString(Constants.subscription);
    return subscription;
  }








}