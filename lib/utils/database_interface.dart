import 'package:akidemic_life/models/post.dart';

class DatabaseInterface {

  static List<Post> getBlogPosts() {

    List<Post> posts = [];

    posts.add(Post(title: 'title 1', img: 'https://i.ibb.co/z5g98qc/water-wallpaper.jpg', description: 'This is a description', postURL: 'https://www.google.com'));

    posts.add(Post(title: 'title 2', img: 'https://i.ibb.co/z5g98qc/water-wallpaper.jpg', description: 'This is a description', postURL: 'https://www.google.com'));

    posts.add(Post(title: 'title 3', img: 'https://i.ibb.co/z5g98qc/water-wallpaper.jpg', description: 'This is a description', postURL: 'https://www.google.com'));

    posts.add(Post(title: 'title 4', img: 'https://i.ibb.co/z5g98qc/water-wallpaper.jpg', description: 'This is a description', postURL: 'https://www.google.com'));

    return posts;

  }


}