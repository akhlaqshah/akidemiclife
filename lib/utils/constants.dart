class Constants {

  static List<String> navigationListItem = ['Find your village', 'Share your knowledge', 'Blogs',
  'About us', 'Admin Page', 'Delete Post', 'Logout'];

  static List<String> userNavigationListItem = ['Find your village', 'Share your knowledge', 'Blogs',
    'About us', 'Logout'];

  static String whoWeAre = 'AKIDemicLife is underpinned by passion, strong values and a focus on solutions and. Here we let you know what drives our mission to help empower parents in academia to balance work with caring responsibilities. Our mission is to help you find everything in one place. Our mission is to provide you with all resources that are openfor everyone in one place. Besides that, our mission to provide you • Tools that suits your situation. • A supportive communityto help you with your needs. • To give ideas that can help you maximize your family time and family success. About Kirsty Nash In 2018, Kirsty founded aKidemic life to provide parents with academia with the resources to balance work and life after she had a tough introduction to parenthood. The idea for a website in her mind for parents came in her mind after the supports she received from first year of parenting. As with many academics on short-term contracts, Kirsty was offered a job that required relocation to a new city, away from friends and family. Her daughter arrived a year after moving to Hobart. Following the loss of control that parenthood brings and without a strong support network Kirsty suffered from post-natal depression. Plans to return to work full-time after 6 months were shelved and trips to conferences cancelled. It was only due to the great help she received from both health professionals and colleagues that she got back on track and was able to return to research part time. aKIDemic Life is her way of paying forward the help she received at a very difficult time.';
  static String aboutTheDevelopers = 'The developer team consisted of some young ardent tech-enthusiasts who studied at University of Tasmania. What started as a project in their last year became their passion over the time. The young students were Jisan Al Riyadh, Salman Faruk Siyam, SebgatullahMaruf, RentaroYamamoto, SunhoAhn, Nhan Nguyen, Clancy Nent. The developers merged their ideas with the needs of Kirsty and implemented everything on the application and that’s how AKIDemic Life app came into being. The valuable comments and simultaneous feedback by Kirsty Nash on the development process of the app. Each of these students worked hard on this project managing all their life struggles and other subjects and was able to build this app by themselves making Kirsty proud and obviously they can be taken as a great example for students who wants to work in the app development area besides their study.';

  static String appName = 'AKIDemic';
  static String usernameValue = '';
  static String picUrl = '';
  static String adminKey;
  static int buttonColor = 0xff464f5d;


  static String uidValue;

  static String uid = 'uid';  //firebase database user id

  static String fullName = 'fullName';
  static String phoneNo = 'phoneNo';
  static String email = 'email';
  static String instituteName = 'instituteName';
  static String location = 'location';
  static String subscription = 'subscription';

  static String loginStatus = 'loginStatus';
  static String rememberUser = 'rememberUser';

  static String userEmail = '';


}