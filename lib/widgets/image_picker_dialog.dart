import 'dart:io';
import 'package:akidemic_life/utils/globals.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';


class ImagePickerDialog extends StatefulWidget {

  @override
  _ImagePickerDialogState createState() => _ImagePickerDialogState();
}

class _ImagePickerDialogState extends State<ImagePickerDialog> {

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        /*...*/

                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      body: imagePickerDialog,
    );
  }

  Future getImageFromCamera() async {

    ImagePicker imagePicker = ImagePicker();
    var image = await imagePicker.getImage(source: ImageSource.camera);

    if(image != null) {
      getCropImage(image);
    }


  }
  Future getImageFromGallery() async {

    ImagePicker imagePicker = ImagePicker();

    var image = await imagePicker.getImage(source: ImageSource.gallery);

    if(image != null) {
      getCropImage(image);
    }

  }
  getCropImage(var image) async {

    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));


    if(croppedFile != null) {

      setState(() {
        //_image = image;
        newImage = croppedFile;
      });

    }//end if

  }}



