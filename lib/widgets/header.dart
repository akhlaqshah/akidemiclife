import 'package:akidemic_life/pages/my_profile_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:flutter/material.dart';


class Header extends StatefulWidget {

  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {

  String userName;
  //String picUrl;

  @override
  void initState() {
    super.initState();

    //print('user pic url: ${Constants.picUrl}');
    //print('username value: ${Constants.usernameValue}');
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        InkWell(

          onTap: () {

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MyProfilePage(),
              ),
            );
          },

          child: Column(
            children: <Widget>[
              Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(
                            Constants.picUrl == null ? "https://i.ibb.co/R6DsZ0g/user.png" : Constants.picUrl))),
              ),

              SizedBox(height: 4.0,),

              Text('${Constants.usernameValue}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                  textScaleFactor: 1.2),
            ],
          ),
        ),
      ],
    );
  }



}

