import 'package:akidemic_life/pages/about_us_page.dart';
import 'package:akidemic_life/pages/admin_page.dart';
import 'package:akidemic_life/pages/blogs_page.dart';
import 'package:akidemic_life/pages/deleted_post.dart';
import 'package:akidemic_life/pages/google_places_api_demo.dart';
import 'package:akidemic_life/pages/login_page.dart';
import 'package:akidemic_life/pages/search_page.dart';
import 'package:akidemic_life/pages/share_your_knowledge_page.dart';
import 'package:akidemic_life/pages/webview_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class NavigationList extends StatefulWidget {
  final BuildContext buildContext;

  NavigationList({this.buildContext});

  @override
  _NavigationListState createState() => _NavigationListState();
}

class _NavigationListState extends State<NavigationList> {
  final _auth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
        Constants.userEmail == 'admin123@gmail.com'
            ? Constants.navigationListItem.length
            : Constants.userNavigationListItem.length,
        (int index) {
          return ListTile(
            title: Text(
              '${Constants.userEmail == 'admin123@gmail.com' ? Constants.navigationListItem[index] : Constants.userNavigationListItem[index]}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              //close navigation drawer
              Navigator.pop(context);

              if (Constants.userEmail == 'admin123@gmail.com') {
                adminListNavigation(context, index);
              } else {
                userListNavigation(context, index);
              }
            },
          );
        },
      ),
    );
  }

  signOut() async {
    await _auth.signOut();
  }

  void adminListNavigation(BuildContext context, int index) {
    if (index == 0) {
      setState(() {});

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SearchVillage(),
        ),
      );
    } else if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShareYourKnowledge(),
        ),
      );
    } else if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => WebViewPage(),
        ),
      );
    } else if (index == 3) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AboutUsPage(),
        ),
      );
    } else if (index == 4) {
      /// admin page

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AdminPage(),
        ),
      );
    } else if (index == 5) {
      /// delete post page

      /*Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => GoogleSearchAutoComplete(),
                  ),
                );*/

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DeletedPosts(),
        ),
      );
    } else if (index == 6) {
      signOut();

      Utils.rememberUser(false);

      /// logout
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => LoginPage(),
        ),
      );
    }
  }

  void userListNavigation(BuildContext context, int index) {
    if (index == 0) {
      setState(() {});

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SearchVillage(),
        ),
      );
    } else if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShareYourKnowledge(),
        ),
      );
    } else if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => WebViewPage(),
        ),
      );
    } else if (index == 3) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AboutUsPage(),
        ),
      );
    } else if (index == 4) {
      signOut();

      Utils.rememberUser(false);

      /// logout
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => LoginPage(),
        ),
      );
    }
  }
}
