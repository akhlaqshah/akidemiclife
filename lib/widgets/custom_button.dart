import 'package:flutter/material.dart';


class CustomButton extends StatelessWidget {

  final width;
  final height;
  final Color borderColor;
  final Color bgColor;
  final String text;
  final IconData icon;

  final Function callback;

  CustomButton({this.width, this.height, this.borderColor, this.bgColor, this.text, this.icon, this.callback});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: RaisedButton(
        onPressed: callback,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
            side: BorderSide(color: borderColor)
        ),
        color: bgColor,
        child: Row(
          children: <Widget>[

            Icon(icon, color: Colors.white),
            SizedBox(width: 8.0,),
            Text(
              '$text',
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
          ],
        ),
      ),
    );
  }
}
