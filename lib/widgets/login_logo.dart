import 'package:flutter/material.dart';

class LoginLogo extends StatelessWidget {
  LoginLogo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        /*width: sizeConfig.width(0.42),
              height: sizeConfig.height(0.42),*/
        width: 140.0,
        height: 140.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          image: new DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(
              'assets/images/logo.jpeg',
            ),
          ),
        ),
      ),
    );
  }
}

/*

Container(
width: 240.0,
height: 140.0,
child: Center(
child: Image.asset('assets/images/icon.jpeg',),
),
decoration: BoxDecoration(
color: Colors.lightBlueAccent,
shape: BoxShape.circle,
),
),*/
