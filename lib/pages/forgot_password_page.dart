import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {


  final _auth = FirebaseAuth.instance;

  TextEditingController emailController = new TextEditingController();

  //form validation
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate;
  String email;

  @override
  void initState() {
    super.initState();

    _autoValidate = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(24.0),
            child: Form(
              key: _formKey,
              autovalidate: _autoValidate,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    width: double.maxFinite,
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                          color: Colors.black,
                          width: 2,
                        ),
                      ),
                      onPressed: () {},
                      color: Colors.white,
                      textColor: Colors.white,
                      child: Text(
                        "Forgot Password".toUpperCase(),
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),


                  SizedBox(
                    height: 20.0,
                  ),

                  Text(
                    'Enter your email to reset your password',
                    style: TextStyle(
                      color: Colors.black45,
                      fontSize: 15,
                    ),
                  ),

                  SizedBox(
                    height: 20.0,
                  ),

                  Text(
                    'enter email here',
                    style: TextStyle(
                      color: Colors.green,
                      fontSize: 14,
                    ),
                  ),

                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    validator: (String arg) {
                      if (arg == null || arg == '') {
                        return 'email cannot be empty';
                      }
                      else if(!validateEmail(arg)) {

                        return 'invalid email';
                      }
                      else
                        return null;
                    },
                    onSaved: (String val) {
                      email = val;
                    },

                    decoration: InputDecoration(
                      hintStyle: TextStyle(fontSize: 16),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),

                    ),
                  ),


                  SizedBox(height: 24.0,),

                  Container(
                    width: double.infinity,
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {

                        _validateInputs();

                      },
                      color: Colors.blue,
                      child: Text(
                        "Reset".toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),

                  SizedBox(height: 16.0,),


                  Container(
                    width: double.infinity,
                    child: FlatButton(
                      onPressed: () {

                        Navigator.pop(context);

                      },
                      color: Colors.white,
                      child: Text(
                        "Back".toUpperCase(),
                        style: TextStyle(color: Colors.blue, fontSize: 15,),
                      ),
                    ),
                  )



                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Future<void> resetPassword(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }


  void _validateInputs() async {

    if (_formKey.currentState.validate()) {
      //    If all data are correct then save data to out variables
      _formKey.currentState.save();


      try{

        resetPassword(emailController.text);
        //emailController.clear();

        Fluttertoast.showToast(
            msg: "Reset password email sent !",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0);

      }catch(e) {
        print(e);
      }


    } else {
      //    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }


  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }



}
