import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:akidemic_life/models/NewPost.dart';
import 'package:akidemic_life/models/PlacesResult.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/globals.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'google_places_api_demo.dart';

class FindYourVillagePage extends StatefulWidget {
  @override
  _FindYourVillagePageState createState() => _FindYourVillagePageState();
}

class _FindYourVillagePageState extends State<FindYourVillagePage> {
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllPost');
  FirebaseStorage storage = FirebaseStorage.instance;

  String _chosenValue;
  String _selectedSortType;

  TextEditingController locationController;

  Query _ref;
  bool listVisibility;
  int searchResultsCount;

  /*List<String> eventsList = [
    'Events',
    'Food',
    'Fitness',
    'Hiking and camping',
    'Hunting and fishing',
    'Canoeing',
    'Kayaking and Rafting',
    'Sailing and motorboating',
    'Biking',
    'Rock Climing',
    'Horseback riding',
    'Skiing',
    'Volunteering',
    'Parent Care',
    'Counselling',
    'Health Care',
    'Legal advice',
    'Emplyment',
    'Childcare',
    'playgroups'
  ];*/

  List<String> sortType = ['Newest Post', 'Oldest Post'];
  File _image;

  String url;

  //Flutter google places autocomplete
  static const kGoogleApiKey = 'AIzaSyDmDjb3-Etcxtrti2n5hS2XfmqDFHk8RzY';

  String adminKeyValue;

  List<Map<dynamic, dynamic>> posts = [];

  List<NewPost> newPostsList = [];

  getGooglePlacesSearch() async {}

  @override
  void initState() {
    super.initState();

    _ref = FirebaseDatabase.instance.reference().child('AllPost');
    _permissionRequest();

    locationController = TextEditingController(text: '');

    listVisibility = false;
    searchResultsCount = 0;

    _chosenValue = eventsList[0];
    _selectedSortType = sortType[0];

    displayBannerAImage();
  }

  _buildPostItem(Map post, String location, String category) {
    if (post['location'] == location && post['category'] == category) {
      searchResultsCount++;
      print('search count: $searchResultsCount');
    }

    //return post['id'] == Constants.uidValue && post['images'][0] != null ? InkWell(
    return post['location'] == location
        ? InkWell(
            onTap: () {
              print('post item clicked');

              /*Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PostReviewPage(
                    post: post,
                  ),
                ),
              );*/
            },
            child: Card(
              elevation: 5.0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            post['title'],
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Image.network(
                      post['images'][0] != null ? post['images'][0] : '',
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      post['description'],
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        : Container();
  }

  @override
  Widget build(BuildContext context) {
    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Find Your Village',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),

                /*Center(
                  child: _image == null
                      ? Image.network('https://i.ibb.co/z5g98qc/water-wallpaper.jpg')
                      : Image.file(_image),
                ),*/

                Center(
                  //child: _image == null
                  child: url == null
                      ? Container(
                          width: MediaQuery.of(context).size.width * 0.65,
                          height: MediaQuery.of(context).size.height * 0.16,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(
                              color: Colors.black,
                              width: 2.0,
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(
                                  'https://i.ibb.co/z5g98qc/water-wallpaper.jpg'),
                            ),
                          ),
                        )
                      : Container(
                          width: 110.0,
                          height: 110.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(
                              color: Colors.black,
                              width: 2.0,
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              //image: FileImage(_image)
                              image: NetworkImage('$url'),
                            ),
                          ),
                          //child: Image.file(_image),
                        ),
                ),

                SizedBox(
                  height: 8.0,
                ),

                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com'
                      ? true
                      : false,
                  child: Center(
                    child: FlatButton(
                      color: Color(0xFF0E82C6),
                      textColor: Colors.white,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) =>
                                imagePickerDialog);

                        /*showDialog(
                          context: context,
                          builder: (BuildContext context) => ImagePickerDialog(),
                        );*/
                      },
                      child: Text(
                        'Change Photo',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ),

                Center(
                  child: Text(
                    'Search for support servcies and events in your area',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),
                Text(
                  'Type your location',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                GestureDetector(
                  onTap: () async {
                    PlacesResult placesResult = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GooglePlacesAPIDemo(),
                      ),
                    );

                    setState(() {
                      locationController.text = placesResult.address;
                    });

                    /*Prediction p = await PlacesAutocomplete.show(
                        context: context,
                        apiKey: kGoogleApiKey,
                        mode: Mode.overlay, // Mode.fullscreen
                        language: "en",
                        components: [new Component(Component.country, "pk")]);

                    print('place data: ${p.toString()}');
*/
                  },
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Colors.black,
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        /*Transform.rotate(
                          angle: 45 * pi / 180,
                          child: Icon(
                            Icons.navigation,
                          ),
                        ),
                        SizedBox(
                          width: 8.0,
                        ),*/
                        /*Text(
                          'Location',
                          style: TextStyle(color: Colors.black, fontSize: 18.0),
                        ),*/

                        Expanded(
                          child: TextField(
                            controller: locationController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(top: 16, right: 8),
                              prefixIcon: InkWell(
                                onTap: () async {
                                  PlacesResult placesResult =
                                      await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          GooglePlacesAPIDemo(),
                                    ),
                                  );

                                  setState(() {
                                    locationController.text =
                                        placesResult.address;
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(0.0),
                                  child: Transform.rotate(
                                    angle: 45 * pi / 180,
                                    child: Icon(
                                      Icons.navigation,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),
                Text(
                  'Search for service',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),

                /*Container(

                  width: 300.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.grey,
                      width: 2.0,
                    ),

                  ),


                  child: DropdownButton<String>(
                    value: _chosenValue,
                    underline: Container(), // this is the magic
                    items: <String>['Google', 'Apple', 'Amazon', 'Tesla']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String value) {
                      setState(() {
                        _chosenValue = value;
                      });
                    },
                  ),
                ),

*/

                SizedBox(
                  height: 8.0,
                ),

                Container(
                  width: 340.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.grey,
                      width: 2.0,
                    ),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton(
                        value: _chosenValue,

                        //items: <String>['Google', 'Apple', 'Amazon', 'Tesla']
                        items: eventsList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        onChanged: (String value) {
                          setState(() {
                            _chosenValue = value;
                          });
                        },
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Text(
                        'Sort by: ',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Expanded(
                      flex: 4,
                      child: Container(
                        //width: 170.0,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                            width: 2.0,
                          ),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton(
                              value: _selectedSortType,
                              items: sortType.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: TextStyle(fontSize: 18),
                                  ),
                                );
                              }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  _selectedSortType = value;
                                });
                              },
                              style: Theme.of(context).textTheme.title,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        //width: 110.0,
                        height: 42.0,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              side: BorderSide(color: Colors.orange)),
                          onPressed: () {
                            String locationValue = locationController.text;
                            print('location value: $locationValue');

                            if (locationValue.isNotEmpty &&
                                locationController.text != null) {
                              setState(() {
                                listVisibility = true;
                                fetchPosts();
                              });
                            } else {
                              Fluttertoast.showToast(
                                  msg: 'location cannot be empty !',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.green,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          },
                          color: Colors.orange,
                          textColor: Colors.white,
                          child: Text("Search", style: TextStyle(fontSize: 14)),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 24.0,
                ),
                Text(
                  'We got $searchResultsCount result',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                  ),
                ),

                SizedBox(
                  height: 16.0,
                ),

                //getPostsList(locationController.text, _chosenValue),

                /*

                Visibility(
                  visible: listVisibility,
                  child: FutureBuilder(
                      future: databaseReference.once(),
                      builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          sCount = posts.length;
                          print('posts length: $sCount');
                        }


                        if (snapshot.hasData) {
                          print('location value: ${locationController.text}');
                          posts.clear();

                          Map<dynamic, dynamic> values = snapshot.data.value;
                          values.forEach((key, values) {
                            List<Map<dynamic, dynamic>> tempList = [];
                            posts.add(values);

                            print('post item added: $values');
                            print('posts list length: ${posts.length}');

                            tempList.add(values);
                          });
                          return new ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: posts.length,
                              itemBuilder: (BuildContext context, int index) {
                                return InkWell(
                                  onTap: () {
                                    print('post item clicked');

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            ViewPostPage(post: posts[index],),
                                      ),
                                    );
                                  },
                                  child: Card(
                                    elevation: 5.0,
                                    child: Container(

                                      padding: EdgeInsets.symmetric(
                                          vertical: 16.0, horizontal: 8.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment
                                            .start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Flexible(
                                                child: Text(
                                                  posts[index]['title'],
                                                  style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                              ),

                                            ],
                                          ),
                                          SizedBox(
                                            height: 8.0,
                                          ),


                                          Image.network(
                                            posts[index]['images'][0] != null
                                                ? posts[index]['images'][0]
                                                : '',
                                          ),


                                          SizedBox(
                                            height: 8.0,
                                          ),
                                          Text(
                                            posts[index]['description'],
                                            style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              });
                        }
                        return CircularProgressIndicator();
                      }),
                ),


                */

                Visibility(
                  visible: listVisibility,
                  child: ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      itemCount: newPostsList.length,
                      itemBuilder: (_, index) {
                        return postUI(newPostsList[index]);
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Visibility getPostsList(String location, String category) {
    return Visibility(
      visible: listVisibility,
      child: Container(
        child: FirebaseAnimatedList(
          query: _ref,
          shrinkWrap: true,
          primary: false,
          itemBuilder: (BuildContext context, DataSnapshot snapshot,
              Animation<double> animation, int index) {
            Map post = snapshot.value;
            /*var result = snapshot.value as Iterable;*/

            setState(() {
              print('hello world');
            });

            return _buildPostItem(post, location, category);
          },
        ),
      ),
    );
  }

  searchCallback() {}

  uploadImageToFirebaseStorage(File image) async {
    final databaseReference =
        FirebaseDatabase.instance.reference().child('bannerAImages');

    String uid = Constants.uidValue;
    print('uid got: $uid');

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference =
        storage.ref().child("bannerimages/find_your_village_$uid.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    setState(() {
      url = newUrl;
    });

    databaseReference.child(uid).set({'id': uid, 'value': url});
  } //end uploadImageToFirebaseStorage()

  Future getImageFromCamera() async {
    //ImagePicker imagePicker = ImagePicker();
    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image != null) {
      await getCropImage(image);

      await uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromCamera()

  Future getImageFromGallery() async {
    //ImagePicker imagePicker = ImagePicker();

    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      await getCropImage(image);

      await uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromGallery()

  getCropImage(var image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      setState(() {
        //_image = image;
        _image = croppedFile;
        //newImage = croppedFile;
      });
    } //end if
  } //getCropImage()

  _permissionRequest() async {
    final permissionValidator = EasyPermissionValidator(
      context: context,
      appName: 'Easy Permission Validator',
    );
    var result = await permissionValidator.camera();
    if (result) {
      // Do something;
    }
  }

  void displayBannerAImage() async {
    BannerAImage bannerAImage = await getBannerAImage();

    setState(() {
      url = bannerAImage.value;
      print('banner image: $url');
    });
  }

  Future<BannerAImage> getBannerAImage() async {
    Completer<BannerAImage> completer = new Completer<BannerAImage>();

    var dbRef = FirebaseDatabase.instance.reference().child("bannerAImages");
    dbRef
        .orderByKey()
        .equalTo(Constants.adminKey)
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot.value != null) {
        if (snapshot.value.isNotEmpty) {
          FirebaseDatabase.instance
              .reference()
              .child('bannerAImages')
              .child(Constants.adminKey)
              .once()
              .then((DataSnapshot snapshot) {
            var bannerAImage =
                new BannerAImage.fromJson(snapshot.key, snapshot.value);
            completer.complete(bannerAImage);
          });
        }
      }
    });

    return completer.future;
  }

  void fetchPosts() {
    DatabaseReference postsRef =
        FirebaseDatabase.instance.reference().child('AllPost');

    List<dynamic> list = [];

    postsRef.once().then((DataSnapshot snap) {
      var KEYS = snap.value.keys;
      var DATA = snap.value;

      newPostsList.clear();
      for (var individualKey in KEYS) {
        NewPost post = new NewPost(
            DATA[individualKey]['category'],
            DATA[individualKey]['contactNo'],
            DATA[individualKey]['date'],
            DATA[individualKey]['description'],
            DATA[individualKey]['id'],
            DATA[individualKey]['linkOfWeb'],
            DATA[individualKey]['location'],
            DATA[individualKey]['title'],
            DATA[individualKey]['images'].cast<String>(),

            //check this issue later

            DATA[individualKey]['childFriendly'],
            DATA[individualKey]['parkingValue'],
            DATA[individualKey]['babyChangeFacility'],
            DATA[individualKey]['parentsRoom'],
            DATA[individualKey]['status'],
            DATA[individualKey]['review'],
            DATA[individualKey]['rating'],
            DATA[individualKey]['familyFriendsFeatures']);

        print(
            'image: ${DATA[individualKey]['contactNo']}      : ${DATA[individualKey]['images'].cast<String>()}');
        newPostsList.add(post);
      } //end for

      setState(() {
        searchResultsCount = newPostsList.length;
        print('total posts: ${newPostsList.length}');
      });
    });
  } //end fetchPosts()

  Widget postUI(NewPost post) {
    return InkWell(
      onTap: () {
        /*Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ViewPostPage(post: posts,),
          ),
        );*/
      },
      child: Card(
        elevation: 5.0,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      post.title,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.0,
              ),
              Image.network(
                post.images[0],
              ),
              SizedBox(
                height: 8.0,
              ),
              Text(
                post.description,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BannerAImage {
  final String key;
  String id;
  String value;

  BannerAImage.fromJson(this.key, Map data) {
    id = data['id'];
    if (id == null) {
      id = '';
    }

    value = data['value'];
    if (value == null) {
      value = '';
    }
  }
}
