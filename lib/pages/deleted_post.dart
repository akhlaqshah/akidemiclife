import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';

class DeletedPosts extends StatefulWidget {
  @override
  _DeletedPostsState createState() => _DeletedPostsState();
}

class _DeletedPostsState extends State<DeletedPosts> {
  DatabaseReference _messagesRef;
  final FirebaseDatabase database = FirebaseDatabase();

  @override
  Widget build(BuildContext context) {
    _messagesRef = database.reference().child('deleteQueue');
    return Scaffold(
      appBar: AppBar(title: Text("Deleted Posts")),
      body: Column(
        children: [
          Flexible(
            child: FirebaseAnimatedList(
              query: _messagesRef,
              shrinkWrap: true,
              primary: false,
              itemBuilder: (BuildContext context, DataSnapshot snapshot,
                  Animation<double> animation, int index) {
                Map post = snapshot.value;

                bool noImageAvailable = false;
                String familyFriendsFeatures =
                    post['familyFriendsFeatures'].toString() ?? 'X';
                String imageUrl =
                    'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg';

                if (post['images'] == null) {
                  noImageAvailable = true;
                } else {
                  imageUrl = post['images'][0] ?? post['images'][1];
                }

                return SizeTransition(
                  sizeFactor: animation,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                      borderRadius: BorderRadius.circular(8.0),
                      elevation: 10.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: CachedNetworkImage(
                                imageUrl: imageUrl,
                                height: 200,
                                fit: BoxFit.cover,
                                alignment: Alignment.center,
                                width: double.infinity,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Title : " + post['title'] ?? "No title",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.w500),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Description : " + post['description'] ?? "No description",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Category : " + post['category'] ?? "No description",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Location : " + post['location'].toString() ?? " No Address",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Row(
                              children: [
                                Text("Child firendly: "),
                                post['childFriendly'] == null
                                    ? Text(
                                        'X',
                                        style: textStyle,
                                      )
                                    : post['childFriendly']
                                        ? Icon(Icons.check)
                                        : Text('X', style: textStyle),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Row(
                              children: [
                                Text("Parking value: "),
                                post['parkingValue'] == null
                                    ? Text(
                                        'X',
                                        style: textStyle,
                                      )
                                    : post['parkingValue']
                                        ? Icon(Icons.check)
                                        : Text(
                                            'X',
                                            style: textStyle,
                                          )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Row(
                              children: [
                                Text("Baby change facility: "),
                                post['babyChangeFacility'] == null
                                    ? Text(
                                        'X',
                                        style: textStyle,
                                      )
                                    : post['babyChangeFacility']
                                        ? Icon(Icons.check)
                                        : Text(
                                            'X',
                                            style: textStyle,
                                          )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Row(
                              children: [
                                Text("Parents room: "),
                                post['parentsRoom'] == null
                                    ? Text(
                                        'X',
                                        style: textStyle,
                                      )
                                    : post['parentsRoom']
                                        ? Icon(
                                            Icons.check,
                                            color: Colors.blue,
                                          )
                                        : Text(
                                            'X',
                                            style: textStyle,
                                          )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Row(
                              children: [
                                Text("Family friendly features: "),
                                post['familyFriendsFeatures'] == null
                                    ? Text(
                                        'X',
                                        style: textStyle,
                                      )
                                    : Text("$familyFriendsFeatures")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Row(
                              children: [
                                Text("Reason to delete: "),
                                post['reason'] == null
                                    ? Text(
                                        'X',
                                        style: textStyle,
                                      )
                                    : Text(post['reason'], style: textStyle,)
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  static const textStyle =
      TextStyle(color: Colors.red, fontSize: 18.0, fontWeight: FontWeight.w500);
}
