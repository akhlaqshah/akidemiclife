import 'dart:async';

import 'package:akidemic_life/models/profile_image.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/size_config.dart';
import 'package:akidemic_life/utils/utils.dart';
import 'package:akidemic_life/widgets/header.dart';
import 'package:akidemic_life/widgets/navigation_list.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:akidemic_life/utils/globals.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import 'package:firebase_auth/firebase_auth.dart';


class HomePage extends StatefulWidget {
  final String title;

  HomePage({this.title});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final _auth = FirebaseAuth.instance;
  User loggedInUser;

  VideoPlayerController _controller;



  @override
  void initState() {
    super.initState();

    getFullNameFromSF();
    getEmailValueFromSP();
    getUserImage();
    getCurrentUser();

    _controller = VideoPlayerController.network(
        'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4')
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
  }

  @override
  void didChangeDependencies() {
    sizeConfig = SizeConfig.init(context);
    super.didChangeDependencies();
  }

  void getCurrentUser() {

    try {
      final user = _auth.currentUser;

      if (user != null) {
        loggedInUser = user;

        print(loggedInUser.email);
      }
    }catch(e) {
      print(e);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      appBar: AppBar(title: Text(widget.title), backgroundColor: Color(0xFF0E82C6),),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 32.0,
            ),
            Container(
              /*width: sizeConfig.width(0.42),
              height: sizeConfig.height(0.42),*/
              width: 140.0,
              height: 140.0,
/*

              child: Center(
                  child: Image.asset('assets/images/logo.jpeg')),
              decoration: BoxDecoration(
                color: Colors.lightBlueAccent,
                shape: BoxShape.circle,
              ),
*/

                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(
                            'assets/images/logo.jpeg', ),
                    ),
                ),


            ),


            SizedBox(
              height: 16.0,
            ),
            Text(
              'Empowering parents to navigate life',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: 320.0,
                    height: 220.0,
                    child: Stack(
                      children: <Widget>[
                        VideoPlayer(_controller),
                        PlayPauseOverlay(controller: _controller),


                        /*Align(
                          alignment: Alignment.center,
                          child: IconButton(
                            icon: Icon(Icons.play_circle_filled,),
                            iconSize: 100,
                            onPressed: () {

                              if(_controller.value.isPlaying)
                                _controller.pause();
                              else
                                _controller.play();

                            },
                          ),

                        ),*/


                      ],
                    ),
                  ),

                ),

              ],
            ),

          ],
            ),
          ),
        ),
      ),
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Color(0xFFF9F8F8),
        ),
        child: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Header(),
                decoration: BoxDecoration(
                  color: Color(0xFFF9F8F8),
                ),
              ),
              NavigationList(buildContext: context,),
            ],
          ),
        ),
      ),
    );
  }

  getUserImage() async {

    ProfileImage pic = await getProfilePic();
    setState(() {
      Constants.picUrl = pic.value;
    });
    print('user pid: ${pic.value}');
  }


  Future<ProfileImage> getProfilePic() async {

    Constants.uidValue = await Utils.getUIDFromSF();
    print('uid value in profile pic: ${Constants.uidValue}');


    Completer<ProfileImage> completer = new Completer<ProfileImage>();

    FirebaseDatabase.instance
        .reference()
        .child("profilePic")
        .child(Constants.uidValue)
        .once()
        .then((DataSnapshot snapshot) {
      var profileImage = new ProfileImage.fromJson(snapshot.key, snapshot.value);
      completer.complete(profileImage);
    });

    return completer.future;

  }

  getFullNameFromSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String name = prefs.getString(Constants.fullName);

    Constants.usernameValue = name;
  }

  getEmailValueFromSP() async {

    Constants.uidValue = await Utils.getUIDFromSF();
    print('uid value: ${Constants.uidValue}');

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String email = prefs.getString(Constants.uidValue);

    Constants.userEmail = email;
  }


  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}

class PlayPauseOverlay extends StatefulWidget {

  VideoPlayerController controller;

  PlayPauseOverlay({this.controller});

  @override
  _PlayPauseOverlayState createState() => _PlayPauseOverlayState();
}

class _PlayPauseOverlayState extends State<PlayPauseOverlay> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: widget.controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
            color: Colors.black26,
            child: Center(
              child: Icon(
                Icons.play_arrow,
                color: Colors.white,
                size: 100.0,
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {

            setState(() {
              widget.controller.value.isPlaying ? widget.controller.pause() : widget.controller.play();
            });

          },
        ),
      ],
    );
  }




}
