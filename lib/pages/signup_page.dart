import 'package:akidemic_life/pages/home_page.dart';
import 'package:akidemic_life/pages/loading_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/utils.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:akidemic_life/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';

import 'no_email_verification_page.dart';


class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {

  final _auth = FirebaseAuth.instance;
  final databaseReference = FirebaseDatabase.instance.reference().child('AllUsers');

  TextEditingController fullNameController;
  TextEditingController phoneNoController;
  TextEditingController emailController;
  TextEditingController institutionNameController;
  TextEditingController currentLocationController;
  TextEditingController passwordController;
  TextEditingController confirmPasswordController;

  String uid;
  String fullName, phoneNo, email, instituteName, currentLocation, password, confirmPassword, subscribe;

  //checkbox
  bool checkedValue;

  //form validation
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _autoValidate;
  bool isLoading;


  @override
  void initState() {

    super.initState();

    _autoValidate = false;
    isLoading = false;
    checkedValue = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: _body(),
    );
  }

  SafeArea _body() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Stack(
          children: <Widget>[

            Container(
              padding: EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 24.0,
                    ),
                    Text(
                      'Signup Page',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 26.0,
                      ),
                    ),

                    SizedBox(
                      height: 12.0,
                    ),

                    Text(
                      'Join the AKIDemic Life community and help spread the world',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.lightGreenAccent,
                        fontSize: 18.0,
                      ),
                    ),


                    SizedBox(
                      height: 24.0,
                    ),

                    Text(
                      'Full Name',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),

                    SizedBox(
                      height: 4.0,
                    ),

                    CustomTextField(
                      hintText: '',
                      controller: fullNameController,
                      inputType: TextInputType.text,
                      obscureText: false,
                      onChange: (value) {
                        fullName = value;
                      },
                      validator: (String arg) {
                        if (arg == null || arg == '') {
                          return 'name cannot be empty';
                        } else
                          return null;
                      },
                      onSaved: (String val) {
                        fullName = val;
                      },

                    ),

                    SizedBox(
                      height: 12.0,
                    ),


                    Text(
                      'Phone no',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),

                    SizedBox(
                      height: 4.0,
                    ),

                    CustomTextField(
                      hintText: '',
                      controller: phoneNoController,
                      inputType: TextInputType.phone,
                      obscureText: false,
                      onChange: (value) {
                        phoneNo = value;
                      },
                      validator: (String arg) {
                        if (arg == null || arg == '') {
                          return 'phone number cannot be empty';
                        } else
                          return null;
                      },
                      onSaved: (String val) {
                        phoneNo = val;
                      },


                    ),


                    SizedBox(
                      height: 12.0,
                    ),

                    Text(
                      'Email',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),

                    SizedBox(
                      height: 4.0,
                    ),

                    CustomTextField(
                      hintText: '',
                      controller: emailController,
                      inputType: TextInputType.emailAddress,
                      obscureText: false,
                      onChange: (value) {
                        email = value;
                      },
                      validator: (String arg) {
                        if (arg == null || arg == '') {
                          return 'email cannot be empty';
                        } else
                          return null;
                      },
                      onSaved: (String val) {
                        email = val;
                      },

                    ),



                    SizedBox(
                      height: 12.0,
                    ),

                    Text(
                      'Institution Name (Optional)',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),

                    SizedBox(
                      height: 4.0,
                    ),

                    CustomTextField(
                      hintText: '',
                      controller: institutionNameController,
                      inputType: TextInputType.text,
                      obscureText: false,
                      onChange: (value) {
                        instituteName = value;
                      },
                      /*validator: (String arg) {
                        if (arg == null || arg == '') {
                          return 'institute name cannot be empty';
                        } else
                          return null;
                      },
                      onSaved: (String val) {
                        instituteName = val;
                      },*/
                    ),



                    SizedBox(
                      height: 12.0,
                    ),

                    Text(
                      'Type your current location',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),

                    SizedBox(
                      height: 4.0,
                    ),

                    CustomTextField(
                      hintText: '',
                      controller: currentLocationController,
                      inputType: TextInputType.text,
                      obscureText: false,
                      onChange: (value) {
                        currentLocation = value;
                      },
                      validator: (String arg) {
                        if (arg == null || arg == '') {
                          return 'location cannot be empty';
                        } else
                          return null;
                      },
                      onSaved: (String val) {
                        currentLocation = val;
                      },
                    ),


                    SizedBox(
                      height: 12.0,
                    ),

                    Text(
                      'Password',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),


                    SizedBox(
                      height: 4.0,
                    ),

                    CustomTextField(
                      hintText: '',
                      controller: passwordController,
                      inputType: TextInputType.visiblePassword,
                      obscureText: true,
                      onChange: (value) {
                        password = value;
                      },
                      validator: (String arg) {
                        if (arg == null || arg == '') {
                          return 'password cannot be empty';
                        }
                        else if(arg.length <= 6) {
                          return 'password length must be greator than 6';
                        }else
                          return null;
                      },
                      onSaved: (String val) {
                        password = val;
                      },

                    ),




                    SizedBox(
                      height: 12.0,
                    ),

                    Text(
                      'Confirm Password',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),

                    SizedBox(
                      height: 4.0,
                    ),

                    CustomTextField(
                      hintText: '',
                      controller: confirmPasswordController,
                      inputType: TextInputType.visiblePassword,
                      obscureText: true,
                      onChange: (value) {
                        confirmPassword = value;
                      },
                      validator: (String arg) {
                        if (arg == null || arg == '') {
                          return 'confirm password cannot be empty';
                        }
                        else if(arg.length <= 6) {
                          return 'confirm password length must be greator than 6';
                        }
                        else if(arg != password) {
                          return 'password did not matched !';
                        }
                        else
                          return null;
                      },
                      onSaved: (String val) {
                        confirmPassword = val;
                      },

                    ),


                    ListTileTheme(
                      contentPadding: EdgeInsets.all(0),
                      child: CheckboxListTile(
                        title: Text('subscribe the website'),
                        value: checkedValue,
                        onChanged: (newValue) {
                          setState(() {
                            checkedValue = newValue;

                            subscribe = checkedValue == true ? 'yes' : 'no';
                          });

                          //checkedValue = newValue;

                        },

                        controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                      ),
                    ),


                    CustomButton(width: 140.0, height: 50.0, borderColor: Color(Constants.buttonColor), bgColor: Color(Constants.buttonColor),
                        text: 'SIGNUP', icon: Icons.send, callback: () => registerCallback()),


                  ],
                ),
              ),
            ),

            Visibility(
              visible: isLoading,
              child: Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
                ),
              ),
            ),

          ],
        ),


      ),
    );
  }

  registerCallback() async {

    //print('full name: $fullName email: $email subscribe: $checkedValue');


    /*if(password.length <= 6) {

      //password length must be more than 6 characters
      Fluttertoast.showToast(
          msg: "Password length must be more than 6 characters !",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
    else if(password != confirmPassword) {

      //password did not matched
      Fluttertoast.showToast(
          msg: "Password mis-matched !",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {*/

      //registerUser();
      _validateInputs();

    //}

  }//end performRegistration()

  void _validateInputs() async {

    if (_formKey.currentState.validate()) {
      //    If all data are correct then save data to out variables
      _formKey.currentState.save();

      setState(() {
        isLoading = true;
      });

      //registerCallback();
      registerUser();

    } else {
      //    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void registerUser() async {

    //instituteName = instituteName == '' ? ' ' : institutionNameController.text;
    print('institute name $instituteName');
    if(instituteName == null || instituteName == '') {
      instituteName = '';
    }


    try {
      final newUser = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);


      uid = newUser.user.uid;
      databaseReference.child(uid).set({

        'fullName':fullName,
        'instituteName':instituteName,
        'location':currentLocation,
        'phoneNumber':phoneNo,
        'subscribe':subscribe,
        'userId':uid
      });

      try {
        newUser.user.sendEmailVerification();
      }
      catch(e) {
        print(e.getMessage());
      }




      if(newUser != null) {

        setState(() {
          isLoading = false;
        });

        addUserDataToSF();

        Fluttertoast.showToast(
            msg: 'User created successfuly and logged in !',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0);


        /*Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(title: 'AKIDemic Life',),
          ),
        );*/

        var user = FirebaseAuth.instance.currentUser;
        if (user.emailVerified)
        {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => HomePage(title: 'AKIDemic Life',),
            ),
          );

        }
        else
        {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => NoEmailVerificationPage(name: fullName, phoneNo: phoneNo,
                  newUser: newUser),
            ),
          );
        }


      }

    }catch(error) {

      setState(() {
        isLoading = false;
      });

      print(error);

      Fluttertoast.showToast(
          msg: error.message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);

    }

  }//end registerUser()


  void addUserDataToSF() {

    Utils.addValueToSF(Constants.uid, uid);

    Utils.addValueToSF(Constants.fullName, fullName);
    Utils.addValueToSF(Constants.phoneNo, phoneNo);
    Utils.addValueToSF(Constants.email, email);
    Utils.addValueToSF(Constants.instituteName, instituteName);
    Utils.addValueToSF(Constants.location, currentLocation);
    Utils.addValueToSF(Constants.subscription, subscribe);

  }



}
