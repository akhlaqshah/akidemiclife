import 'package:akidemic_life/pages/login_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_page.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}


class _SplashScreenState extends State<SplashScreen> {

  String adminKeyValue;


  @override
  void initState() {
    super.initState();

    //assignAdminUidValue();
    getAdminUid();
    print('admin key in init: ${Constants.adminKey}');

    getRememberMeStatus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SafeArea(

        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),

    );
  }


  getRememberMeStatus() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool(Constants.rememberUser);

    print('login status: $boolValue');

    if(boolValue != null && boolValue ) {

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => HomePage(
            title: Constants.appName,
          ),
        ),
      );

    }
    else {

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => LoginPage(),
        ),
      );

    }

  }//end getRememberMeStatus()



  assignAdminUidValue() {

    getAdminUid().then((value) => (){


      print('admin key in loading: $value');
      Constants.adminKey = value;

    });

  }

  Future<String> getAdminUid() async {

    var dbRef = FirebaseDatabase.instance.reference().child("adminKey");

    dbRef.once().then((DataSnapshot snapshot) async {

      if (snapshot.value != null)  {
        if (snapshot.value.isNotEmpty) {

          await dbRef.child('id')
              .once()
              .then((DataSnapshot snapshot) {

              Constants.adminKey = snapshot.value;
              adminKeyValue = snapshot.value;

              print('admin key in loading: ${Constants.adminKey}');

          });

        }
        else {
          print('snapshot empty');
        }
      }
      else {
        print('snapshot null');
      }

    });

    return adminKeyValue;

  }//end getAdminUid()



}
