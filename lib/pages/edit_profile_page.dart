import 'dart:async';
import 'dart:io';
import 'package:akidemic_life/models/user_data.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:akidemic_life/widgets/custom_text_field.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class EditProfilePage extends StatefulWidget {
  UserData userData;
  //Function fetchDataFromFirebaseDatabase;

  //EditProfilePage({@required this.userData, this.fetchDataFromFirebaseDatabase});
  EditProfilePage({@required this.userData});

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllUsers');

  TextEditingController fullNameController;
  TextEditingController phoneNoController;
  TextEditingController institutionNameController;
  TextEditingController locationController;

  bool progressVisibility;

  @override
  void initState() {
    super.initState();

    setState(() {
      progressVisibility = false;
    });

    fullNameController = TextEditingController(text: widget.userData.fullName);
    phoneNoController = TextEditingController(text: widget.userData.phoneNo);
    institutionNameController =
        TextEditingController(text: widget.userData.instituteName);
    locationController = TextEditingController(text: widget.userData.location);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
          onWillPop: _willPopCallback,
          child: Scaffold(
        backgroundColor: Color(0xFF0E82C6),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        'Edit Profile',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 32.0,
                      ),
                      Center(
                        child: Container(
                          width: 140.0,
                          height: 140.0,
                          decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage(
                                'assets/images/logo.jpeg',
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        'Full Name',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                        ),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      CustomTextField(
                        hintText: '',
                        controller: fullNameController,
                        inputType: TextInputType.text,
                        obscureText: false,
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Text(
                        'Phone no',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                        ),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      CustomTextField(
                        hintText: '',
                        controller: phoneNoController,
                        inputType: TextInputType.text,
                        obscureText: false,
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Text(
                        'Institution Name (Optional)',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                        ),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      CustomTextField(
                        hintText: '',
                        controller: institutionNameController,
                        inputType: TextInputType.text,
                        obscureText: false,
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Text(
                        'Type your current location',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                        ),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      CustomTextField(
                        hintText: '',
                        controller: locationController,
                        inputType: TextInputType.text,
                        obscureText: false,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          CustomButton(
                              width: 140.0,
                              height: 50.0,
                              borderColor: Color(Constants.buttonColor),
                              bgColor: Color(Constants.buttonColor),
                              text: 'Update'.toUpperCase(),
                              icon: Icons.send,
                              callback: () => performUpdate()),
                          CustomButton(
                              width: 140.0,
                              height: 50.0,
                              borderColor: Color(Constants.buttonColor),
                              bgColor: Color(Constants.buttonColor),
                              text: 'Cancel'.toUpperCase(),
                              icon: Icons.close,
                              callback: () {
                                
                                UserData userData = UserData(
                                    fullNameController.text,
                                    phoneNoController.text,
                                    institutionNameController.text,
                                    locationController.text,
                                    '');

                                Navigator.pop(context, userData);
                              }),
                        ],
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: progressVisibility,
                  child: Positioned.fill(
                    child: Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  performUpdate() {
    setState(() {
      progressVisibility = true;
    });

    Timer(Duration(seconds: 3), () {
      databaseReference.child(Constants.uidValue).update({
        'fullName': fullNameController.text,
        'phoneNumber': phoneNoController.text,
        'instituteName': institutionNameController.text,
        'location': locationController.text,
      });

      setState(() {
        progressVisibility = false;
      });

      //widget.fetchDataFromFirebaseDatabase();

      UserData userData = UserData(
          fullNameController.text,
          phoneNoController.text,
          institutionNameController.text,
          locationController.text,
          '');

      Fluttertoast.showToast(
          msg: 'Profile information updated successfully !',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);

      Navigator.pop(context, userData);
    });
  }


  Future<bool> _willPopCallback() async {
    // await showDialog or Show add banners or whatever
    // then
    return false; // return true if the route to be popped
}
}
