import 'package:akidemic_life/pages/admin_view_post_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  Query _ref;

  Map post1;
  Map post2;
  

  @override
  void initState() {
    super.initState();

    _ref = FirebaseDatabase.instance.reference().child('AllPost');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF0E82C6),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),

                  Text(
                    'All Post',
                    style: TextStyle(color: Colors.black, fontSize: 20.0),
                  ),

                  SizedBox(
                    height: 20.0,
                  ),

                  Text(
                    'Posts under review',
                    style: TextStyle(color: Colors.black, fontSize: 14.0),
                  ),

                  SizedBox(
                    height: 10.0,
                  ),

                  //unapproved posts list
                  Container(
                    child: FirebaseAnimatedList(
                      query: _ref,
                      shrinkWrap: true,
                      primary: false,
                      reverse: true,
                      itemBuilder: (BuildContext context, DataSnapshot snapshot,
                          Animation<double> animation, int index) {
                        //Map post = snapshot.value;
                        post1 = snapshot.value;

                        print('unapproved posts: $post1');

                        //return _buildPostItem(post, index);
                        return _buildPostItem(post1, index);
                      },
                    ),
                  ),

                  SizedBox(
                    height: 20.0,
                  ),

                  Text(
                    'All approved post',
                    style: TextStyle(color: Colors.black, fontSize: 14.0),
                  ),

                  SizedBox(
                    height: 10.0,
                  ),

                  //approved posts list
                  Container(
                    child: FirebaseAnimatedList(
                      query: _ref,
                      shrinkWrap: true,
                      primary: false,
                      reverse: true,
                      itemBuilder: (BuildContext context, DataSnapshot snapshot,
                          Animation<double> animation, int index) {
                        if (snapshot.value != null) {
                          //Map post = snapshot.value;
                          post2 = snapshot.value;

                          //return _buildApprovedPostItem(post);
                          return _buildApprovedPostItem(post2);
                        }

                        return Container();
                      },
                    ),
                  ),

                  //un-approved posts list
                ],
              ),
            ),
          ),
        ));
  }

  _buildPostItem(Map post, int index) {
    if (post['status'] == 'unapproved') {
      print("unapproved posts ${post['unapproved']}");
    }


    print('post size: ${post.length}');
    List<dynamic> imagesListValues = post['images'];

    String imageUrl =
        'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg';
    if (imagesListValues != null) {
      imageUrl =
          imagesListValues[0] ?? imagesListValues[imagesListValues.length - 1];
    }
  



    //return post['id'] == Constants.uidValue && post['images'][0] != null ? InkWell(
    return post['status'] == 'unapproved'
        ? InkWell(
            onTap: () async {
              print('post item clicked');

              int value = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AdminViewPostPage(
                    postData: post,
                  ),
                ),
              );

              if (value == 1) {
                Navigator.pop(context);
              } else if (value == 2) {
                //update both lists

                setState(() {
                  post2.addAll(post);
                  //post.clear();
                });
              }
            },
            child: Card(
              elevation: 5.0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    
                    Text(
                      post['title'] ?? "No title",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    CachedNetworkImage(
                      imageUrl: imageUrl,
                      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) =>
                          Image.asset('assets/images/placeholder.png'),
                      width: double.infinity,
                      height: 200,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      post['description'] ?? "No description ",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        : Container();
  }

  _buildApprovedPostItem(Map post) {
    print("approved posts ${post['approved']}");

    String imageUrl =
        'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg';

    if (post['images'] != null) {
      imageUrl = post['images'][0] ?? post['images'][1];
    }

    //return post['id'] == Constants.uidValue && post['images'][0] != null ? InkWell(
    return post['status'] != null
        ? post['status'] == 'approved'
            ? InkWell(
                onTap: () async {
                  print('post item clicked');

                  int value = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AdminViewPostPage(
                        postData: post,
                      ),
                    ),
                  );

                  if (value == 1) {
                    Navigator.pop(context);
                  }
                },
                child: Card(
                  elevation: 5.0,
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text(
                                post['title'] ?? "No Title",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        CachedNetworkImage(
                          imageUrl: imageUrl,
                          placeholder: (context, url) => Container(
                              width: 50,
                              height: 50,
                              child: CircularProgressIndicator()),
                          errorWidget: (context, url, error) =>
                              Image.asset('assets/images/placeholder.png'),
                          width: double.infinity,
                          height: 200,
                          fit: BoxFit.cover,
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        Text(
                          post['description'] ?? "no description",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : Container()
        : Text("Status unknown");
  }
}
