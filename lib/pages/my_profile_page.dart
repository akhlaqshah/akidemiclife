import 'dart:async';
import 'dart:io';
import 'package:akidemic_life/models/post.dart';
import 'package:akidemic_life/models/user_data.dart';
import 'package:akidemic_life/pages/edit_profile_page.dart';
import 'package:akidemic_life/pages/forgot_password_page.dart';
import 'package:akidemic_life/pages/post_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:akidemic_life/utils/database_interface.dart';
import 'package:firebase_storage/firebase_storage.dart';

class MyProfilePage extends StatefulWidget {
  @override
  _MyProfilePageState createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage>
    with WidgetsBindingObserver {
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllUsers');
  FirebaseStorage storage = FirebaseStorage.instance;

  Query _ref;

  File _image;
  List<Post> posts;

  //firebase data
  String fullName, phoneNo, instituteName, location, subscribe, userId;
  var userData;

  String url;

  @override
  void initState() {
    super.initState();

    _ref = FirebaseDatabase.instance.reference().child('AllPost');

    _permissionRequest();

    posts = DatabaseInterface.getBlogPosts();

    //old method call not efficient
    // getUserInfoFromFirebaseDb();

    /**start**/ //new method call

    displayUserData();

    displayProfilePic();

    print('profile pic url: $url');

    /**end**/ //new method call
  }

  void displayUserData() async {
    UserInfo userInfo = await getUserInfo();

    setState(() {
      fullName = userInfo.fullName;
      phoneNo = userInfo.phoneNumber;
      instituteName = userInfo.instituteName;
      location = userInfo.location;
    });
  }

  _buildPostItem(Map post) {
    if (post['id'] == Constants.uidValue) {
      //print("first image ${post['images'][0]}");
    }

    String imageUrl;

    if (post['images'] != null) {
      imageUrl = post['images'][0] ??
          post['images'][1];
    }
    //return post['id'] == Constants.uidValue && post['images'][0] != null ? InkWell(
    return post['id'] == Constants.uidValue
        ? InkWell(
            onTap: () async {
              print('post item clicked');

              int value = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PostPage(
                    postData: post,
                  ),
                ),
              );

              if (value == 1) {
                Navigator.pop(context);
              }
            },
            child: Card(
              elevation: 5.0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            post['title'],
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Image.network(
                      
                      imageUrl ?? 'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg',
                      height: 200,
                      fit: BoxFit.cover,
                      width: double.infinity,
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      post['description'],
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        : Container();
  }

  @override
  Widget build(BuildContext context) {
    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),

                Center(
                  child: Text(
                    'My Profile',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 26.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),

                SizedBox(
                  height: 16.0,
                ),

                Center(
                  //child: _image == null
                  child: url == null
                      ? Container(
                          width: 110.0,
                          height: 110.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.black,
                              width: 2.0,
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(
                                  'https://i.ibb.co/z5g98qc/water-wallpaper.jpg'),
                            ),
                          ),
                        )
                      : Container(
                          width: 110.0,
                          height: 110.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.black,
                              width: 2.0,
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              //image: FileImage(_image)
                              image: NetworkImage('$url'),
                            ),
                          ),
                          //child: Image.file(_image),
                        ),
                ),

                //https://i.ibb.co/z5g98qc/water-wallpaper.jpg

                SizedBox(
                  height: 8.0,
                ),
                Center(
                  child: FlatButton(
                    color: Color(0xFF0E82C6),
                    textColor: Colors.white,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => imagePickerDialog);
                    },
                    child: Text(
                      'Change Picture',
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),

                SizedBox(
                  height: 16.0,
                ),

                Row(
                  children: <Widget>[
                    Text(
                      'Username : ',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Text(
                      '$fullName',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    SizedBox(
                      width: 80.0,
                    ),
                    InkWell(
                      onTap: () async {
                        UserData userData = UserData(
                            fullName, phoneNo, instituteName, location, userId);

                        /*Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditProfilePage(userData: userData, fetchDataFromFirebaseDatabase: getUserInfoFromFirebaseDb(),),
                          ),
                        );*/

                        userData = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                EditProfilePage(userData: userData),
                          ),
                        );

                        setState(() {
                          fullName = userData.fullName;
                          phoneNo = userData.phoneNo;
                          instituteName = userData.instituteName;
                          location = userData.location;
                        });
                      },
                      child: Text(
                        'Edit',
                        style: TextStyle(
                          color: Colors.deepPurpleAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 16.0,
                ),

                Row(
                  children: <Widget>[
                    Text(
                      'User phone : ',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Text(
                      '$phoneNo',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    SizedBox(
                      width: 80.0,
                    ),
                    InkWell(
                      onTap: () async {
                        UserData userData = UserData(
                            fullName, phoneNo, instituteName, location, userId);

                        /*Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditProfilePage(userData: userData),
                          ),
                        );*/

                        userData = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                EditProfilePage(userData: userData),
                          ),
                        );

                        setState(() {
                          fullName = userData.fullName;
                          phoneNo = userData.phoneNo;
                          instituteName = userData.instituteName;
                          location = userData.location;
                        });
                      },
                      child: Text(
                        'Edit',
                        style: TextStyle(
                          color: Colors.deepPurpleAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 16.0,
                ),

                Row(
                  children: <Widget>[
                    Text(
                      'Institute Name : ',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Text(
                      '$instituteName',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    SizedBox(
                      width: 40.0,
                    ),
                    InkWell(
                      onTap: () async {
                        UserData userData = UserData(
                            fullName, phoneNo, instituteName, location, userId);

                        /*Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditProfilePage(userData: userData),
                          ),
                        );*/

                        userData = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                EditProfilePage(userData: userData),
                          ),
                        );

                        setState(() {
                          fullName = userData.fullName;
                          phoneNo = userData.phoneNo;
                          instituteName = userData.instituteName;
                          location = userData.location;
                        });
                      },
                      child: Text(
                        'Edit',
                        style: TextStyle(
                          color: Colors.deepPurpleAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 16.0,
                ),

                Row(
                  children: <Widget>[
                    Text(
                      'Location : ',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Text(
                      '$location',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    SizedBox(
                      width: 40.0,
                    ),
                    InkWell(
                      onTap: () async {
                        UserData userData = UserData(
                            fullName, phoneNo, instituteName, location, userId);

                        /*Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditProfilePage(userData: userData),
                          ),
                        );*/

                        userData = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                EditProfilePage(userData: userData),
                          ),
                        );

                        setState(() {
                          fullName = userData.fullName;
                          phoneNo = userData.phoneNo;
                          instituteName = userData.instituteName;
                          location = userData.location;
                        });
                      },
                      child: Text(
                        'Edit',
                        style: TextStyle(
                          color: Colors.deepPurpleAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 24.0,
                ),

                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ForgotPasswordPage(),
                      ),
                    );
                  },
                  child: Text(
                    'Change Password?',
                    style: TextStyle(
                      color: Colors.deepPurple,
                      fontSize: 22.0,
                    ),
                  ),
                ),

                SizedBox(
                  height: 32.0,
                ),

                Text(
                  'My Posts',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 22.0,
                  ),
                ),

                SizedBox(
                  height: 16.0,
                ),

                Container(
                  child: FirebaseAnimatedList(
                    query: _ref,
                    shrinkWrap: true,
                    primary: false,
                    itemBuilder: (BuildContext context, DataSnapshot snapshot,
                        Animation<double> animation, int index) {
                      Map post = snapshot.value;

                      //print(post['images'][0]);

                      return _buildPostItem(post);
                    },
                  ),
                ),

/*

                ListView.builder(
                  itemCount: posts.length,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        print('post item clicked');

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PostPage(),
                          ),
                        );
                      },
                      child: Card(
                        elevation: 5.0,
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 16.0, horizontal: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    '${posts[index].getTitle()}',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.blue,
                                    ),
                                  ),
                                  Center(
                                    child: FlatButton(
                                      color: Colors.white,
                                      textColor: Colors.white,
                                      disabledColor: Colors.grey,
                                      disabledTextColor: Colors.black,
                                      padding: EdgeInsets.all(8.0),
                                      splashColor: Colors.blueAccent,
                                      onPressed: () {
                                        ///delete post
                                        print('post item clicked');
                                      },
                                      child: Text(
                                        'delete',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.red,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Image.network(
                                'https://i.ibb.co/z5g98qc/water-wallpaper.jpg',
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Text(
                                '${posts[index].getDescription()}',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),

*/
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future getImageFromCamera() async {
    //ImagePicker imagePicker = ImagePicker();

    //var image = await imagePicker.getImage(source: ImageSource.camera);
    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image != null) {
      getCropImage(image);

      uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromCamera()

  Future getImageFromGallery() async {
    //ImagePicker imagePicker = ImagePicker();
    //var image;
    File image;

    try {
      //image = await imagePicker.getImage(source: ImageSource.gallery);
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
    } catch (e) {
      print(e);
    }

    if (image != null) {
      //getCropImage(image, no);
      getCropImage(image);

      uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromGallery()

  getCropImage(var image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      setState(() {
        ///prev code
        _image = croppedFile;
      });
    } //end if
  } //getCropImage()

  _permissionRequest() async {
    final permissionValidator = EasyPermissionValidator(
      context: context,
      appName: 'Easy Permission Validator',
    );
    var result = await permissionValidator.camera();
    if (result) {
      // Do something;
    }
  }

  /*getDataCallback() {
      getUserInfoFromFirebaseDb();
  }*/

  getUserInfoFromFirebaseDb() async {
    //String uid = await Utils.getUIDFromSF();
    String uid = Constants.uidValue;
    userId = uid;

    print('uid value: $uid');

    var dbRef = FirebaseDatabase.instance.reference().child("AllUsers");
    dbRef.orderByKey().equalTo(uid).once().then((DataSnapshot snapshot) {
      if (snapshot.value != null) {
        if (snapshot.value.isNotEmpty) {
          var ref = FirebaseDatabase.instance.reference().child('AllUsers');
          ref.child(uid).once().then((DataSnapshot snapshot) {
            print(snapshot.value);
            snapshot.value.forEach((key, values) {
              if (key == 'location') {
                print(values);
                setState(() {
                  location = values;
                });
              }

              if (key == 'instituteName') {
                print(values);
                setState(() {
                  instituteName = values;
                });
              }

              if (key == 'fullName') {
                print(values);
                setState(() {
                  fullName = values;
                });
              }

              if (key == 'phoneNumber') {
                print(values);
                setState(() {
                  phoneNo = values;
                });
              }
            });
          });
        }
      }
    });
  }

  updateValues(String newName, newPhone, newInstitute, newLocation) {
    setState(() {
      fullName = newName;
      phoneNo = newPhone;
      instituteName = newInstitute;
      location = newLocation;
    });
  }

  Future<UserInfo> getUserInfo() async {
    print('uid value ${Constants.uidValue}');

    Completer<UserInfo> completer = new Completer<UserInfo>();

    FirebaseDatabase.instance
        .reference()
        .child("AllUsers")
        .child(Constants.uidValue)
        .once()
        .then((DataSnapshot snapshot) {
      var userInfo = new UserInfo.fromJson(snapshot.key, snapshot.value);
      completer.complete(userInfo);
    });

    return completer.future;
  }

  Future<ProfilePic> getProfileImage() async {
    Completer<ProfilePic> completer = new Completer<ProfilePic>();

    var dbRef = FirebaseDatabase.instance.reference().child("profilePic");
    dbRef
        .orderByKey()
        .equalTo(Constants.uidValue)
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot.value != null) {
        if (snapshot.value.isNotEmpty) {
          FirebaseDatabase.instance
              .reference()
              .child("profilePic")
              .child(Constants.uidValue)
              .once()
              .then((DataSnapshot snapshot) {
            var profilePic =
                new ProfilePic.fromJson(snapshot.key, snapshot.value);
            completer.complete(profilePic);
          });
        }
      }
    });

    /*
    FirebaseDatabase.instance
        .reference()
        .child("profilePic")
        .child(Constants.uidValue)
        .once()
        .then((DataSnapshot snapshot) {

          if(snapshot != null) {
            if(snapshot.value != null) {
              var profilePic = new ProfilePic.fromJson(snapshot.key, snapshot.value);
              completer.complete(profilePic);
            }
          }
    });*/

    return completer.future;
  }

  void displayProfilePic() async {
    ProfilePic profilePic = await getProfileImage();

    setState(() {
      url = profilePic.value;
    });
  }

  //uploadImageToFirebaseStorage(var image) async {
  uploadImageToFirebaseStorage(File image) async {
    final databaseReference =
        FirebaseDatabase.instance.reference().child('profilePic');

    String uid = Constants.uidValue;
    print('uid got: $uid');

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference = storage.ref().child("profileimages/$uid.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    setState(() {
      url = newUrl;
    });

    databaseReference.child(uid).set({'id': uid, 'value': url});
  } //end uploadImageToFirebaseStorage()

}

class UserInfo {
  final String key;
  String fullName;
  String instituteName;
  String location;
  String phoneNumber;
  String subscribe;
  String userId;

  UserInfo.fromJson(this.key, Map data) {
    fullName = data['fullName'];
    if (fullName == null) {
      fullName = '';
    }

    instituteName = data['instituteName'];
    if (instituteName == null) {
      instituteName = '';
    }

    location = data['location'];
    if (location == null) {
      location = '';
    }

    phoneNumber = data['phoneNumber'];
    if (phoneNumber == null) {
      phoneNumber = '';
    }

    subscribe = data['subscribe'];
    if (subscribe == null) {
      subscribe = '';
    }

    userId = data['userId'];
    if (userId == null) {
      userId = '';
    }
  }
}

class ProfilePic {
  final String key;
  String id;
  String value;

  ProfilePic.fromJson(this.key, Map data) {
    id = data['id'];
    if (id == null) {
      id = '';
    }

    value = data['value'];
    if (value == null) {
      value = '';
    }
  }
}
