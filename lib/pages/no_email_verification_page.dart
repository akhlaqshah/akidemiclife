import 'package:akidemic_life/pages/login_page.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class NoEmailVerificationPage extends StatefulWidget {

  String name, phoneNo;
  var newUser;

  NoEmailVerificationPage({this.name, this. phoneNo, this.newUser});

  @override
  _NoEmailVerificationPageState createState() =>
      _NoEmailVerificationPageState();
}

class _NoEmailVerificationPageState extends State<NoEmailVerificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

            children: <Widget>[

              headerSection(),
              userInfoSection(),
              logoutButton(context),

            ],
          ),
        ),
      ),
    );
  }

  Container logoutButton(BuildContext context) {
    return Container(
              width: 130.0,
              height: 50.0,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(
                    color: Colors.black,
                    width: 2,
                  ),
                ),
                onPressed: () {
                  /// navigate to login screen

                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginPage(),
                    ),
                  );
                },
                color: Colors.white,
                textColor: Colors.white,
                child: Text(
                  "Logout".toUpperCase(),
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ),
            );
  }
  Column userInfoSection() {
    return Column(
              children: <Widget>[

                Row(
                  children: <Widget>[

                    Text('Name: ', style: TextStyle(fontSize: 15),),
                    Text('${widget.name}', style: TextStyle(fontSize: 15),),

                  ],
                ),

                SizedBox(height: 16.0,),

                Row(
                  children: <Widget>[

                    Text('Contact: ', style: TextStyle(fontSize: 15),),
                    Text('${widget.phoneNo}', style: TextStyle(fontSize: 15),),

                  ],
                ),


              ],
            );
  }
  Column headerSection() {
    return Column(
              children: <Widget>[

                Text(
                  'Email is not activated yet. Please activate it.',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.red, fontSize: 26),
                ),

                SizedBox(height: 8.0,),

                CustomButton(
                  width: 180.0,
                  height: 50.0,
                  borderColor: Colors.deepPurpleAccent,
                  bgColor: Colors.deepPurpleAccent,
                  text: 'Send email'.toUpperCase(),
                  callback: () => sendEmailCallback(),
                ),


              ],
            );
  }

  sendEmailCallback() {

    try {
      //widget.newUser.user.sendEmailVerification();

      var auth = FirebaseAuth.instance.currentUser;
      auth.sendEmailVerification();


      Fluttertoast.showToast(
          msg: 'Verification email sent',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    catch(e) {
      print(e.toString());
    }
  }

}
