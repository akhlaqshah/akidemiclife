import 'package:akidemic_life/models/NewPost.dart';
import 'package:akidemic_life/pages/view_post_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';

class PostDetails extends StatefulWidget {
  final NewPost post;

  const PostDetails({Key key, this.post}) : super(key: key);

  @override
  _PostDetailsState createState() => _PostDetailsState();
}

class _PostDetailsState extends State<PostDetails> {
  bool noImageAvailable = false;
  String imageUrl =
      'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg';
  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.post.images == null) {
      noImageAvailable = true;
    } else {
      imageUrl = widget.post.images[0] ?? widget.post.images[1];
    }

    String familyFriendsFeatures = widget.post.familyFriendsFeatures ?? 'X';

    CollectionReference users = FirebaseFirestore.instance
        .collection('reviews/' + widget.post.date + '/postreviews');
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.post.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Material(
              elevation: 10.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        imageUrl,
                        height: 200,
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                        width: double.infinity,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Text(
                      "Title : " + widget.post.title,
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Text(
                      "Description : " + widget.post.description,
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w400),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Text(
                      "Category : " + widget.post.category,
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w400),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Text(
                      "Location : " + widget.post.location,
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w400),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Row(
                      children: [
                        Text("Child firendly: "),
                        widget.post.childFriendly == null
                            ? Text(
                                'X',
                                style: textStyle,
                              )
                            : widget.post.childFriendly
                                ? Icon(Icons.check)
                                : Text('X', style: textStyle),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Row(
                      children: [
                        Text("Parking value: "),
                        widget.post.parkingValue == null
                            ? Text(
                                'X',
                                style: textStyle,
                              )
                            : widget.post.parkingValue
                                ? Icon(Icons.check)
                                : Text(
                                    'X',
                                    style: textStyle,
                                  )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Row(
                      children: [
                        Text("Baby change facility: "),
                        widget.post.babyChangeFacility == null
                            ? Text(
                                'X',
                                style: textStyle,
                              )
                            : widget.post.babyChangeFacility
                                ? Icon(Icons.check)
                                : Text(
                                    'X',
                                    style: textStyle,
                                  )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Row(
                      children: [
                        Text("Parents room: "),
                        widget.post.parentsRoom == null
                            ? Text(
                                'X',
                                style: textStyle,
                              )
                            : widget.post.parentsRoom
                                ? Icon(
                                    Icons.check,
                                    color: Colors.blue,
                                  )
                                : Text(
                                    'X',
                                    style: textStyle,
                                  )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Row(
                      children: [
                        Text("Family friendly features: "),
                        widget.post.familyFriendsFeatures == null
                            ? Text(
                                'X',
                                style: textStyle,
                              )
                            : Text("$familyFriendsFeatures")
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "User reviews",
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 18,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: users.snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return Text(
                      'Something went wrong ' + snapshot.error.toString());
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Text("Loading");
                }

                return new ListView(
                  shrinkWrap: true,
                  children: snapshot.data.docs.map((DocumentSnapshot document) {
                    double rating = document.data()['rating'];
                    final DateTime time =
                        (document.data()['timestamp']).toDate();

                    //markAsReed();
                    String timeStamp = DateFormat.yMMMd().add_jm().format(time);

                    return Padding(
                      padding: const EdgeInsets.only(
                          top: 8.0, left: 8.0, right: 8.0),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.0),
                        elevation: 10.0,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                document.data()['user'],
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.blue,
                                    fontWeight: FontWeight.w500),
                              ),
                              RatingBar(
                                initialRating: rating,
                                direction: Axis.horizontal,
                                itemCount: rating.toInt(),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: null,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: Text(
                                  document.data()['comment'],
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.black45,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Text(
                                timeStamp,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black45,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PostReviewPage(
                        post: widget.post,
                      )));
        },
        elevation: 8.0,
        child: Icon(Icons.add),
      ),
    );
  }

  static const textStyle =
      TextStyle(color: Colors.red, fontSize: 18.0, fontWeight: FontWeight.w500);
}
