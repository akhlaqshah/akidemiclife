import 'dart:io';

import 'package:akidemic_life/models/post_image.dart';
import 'package:akidemic_life/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:akidemic_life/utils/globals.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_database/firebase_database.dart';


class AdminEditPostPage extends StatefulWidget {

  final Map postData;
  AdminEditPostPage({this.postData});

  @override
  _AdminEditPostPageState createState() => _AdminEditPostPageState();
}

class _AdminEditPostPageState extends State<AdminEditPostPage> {

  FirebaseStorage storage = FirebaseStorage.instance;

  TextEditingController titleController;
  TextEditingController descriptionController;
  TextEditingController contactNoController;
  TextEditingController websiteController;
  TextEditingController locationController;
  TextEditingController familyFriendlyFeaturesController;

  String _chosenValue;

  int no;

  File _image1,
      _image2,
      _image3,
      _image4,
      _image5,
      _image6,
      _image7,
      _image8,
      _image9,
      _image10;

  DateTime _selectedDate;
  String selectedDateValue;

  String url, postUrl;
  int imageNo;

  List imagesList = [];
  List<PostImage> imagesListToUpload = [];
  //List<PostImage> imagesListToUpload2 = new List(10);
  List<PostImage> imagesListToUpload2 = [null, null, null, null, null, null, null, null, null, null];

  String img1, img2, img3, img4, img5, img6, img7, img8, img9, img10;

  String originalDate;

  //check boxes
  bool childFriendlyValue;
  bool parkingValue;
  bool babyChangeFacility;
  bool parentsRoom;


  @override
  void initState() {
    super.initState();

    childFriendlyValue = false;
    parkingValue = false;
    babyChangeFacility = false;
    parentsRoom = false;

    imageNo = -1;

    _permissionRequest();

    titleController = TextEditingController(text: widget.postData['title']);
    descriptionController =
        TextEditingController(text: widget.postData['description']);
    contactNoController =
        TextEditingController(text: widget.postData['contactNo']);
    websiteController =
        TextEditingController(text: widget.postData['linkOfWeb']);
    locationController =
        TextEditingController(text: widget.postData['location']);
    familyFriendlyFeaturesController =
        TextEditingController(text: widget.postData['familyFriendsFeatures']);

    //_chosenValue = eventsList[0];
    _chosenValue = widget.postData['category'];
    imagesList = widget.postData['images'];



    //imagesListToUpload.addAll(imagesList);
    createImagesListToUpload();

    print('images length: ${imagesList.length}');
    print('images to upload length: ${imagesListToUpload2.length}');
    print('images and id: ${imagesListToUpload2[0].image} and id: ${imagesListToUpload2[0].id}');

    originalDate = widget.postData['date'];

    print('original date: $originalDate');

    selectedDateValue = widget.postData['date'];

    assignImages();
  }

  @override
  Widget build(BuildContext context) {

    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera(0);
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  'Edit post',
                  style: TextStyle(color: Colors.black, fontSize: 20.0),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  width: 340.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.grey,
                      width: 2.0,
                    ),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton(
                        value: _chosenValue,
                        items: eventsList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        onChanged: (String value) {
                          setState(() {
                            _chosenValue = value;
                          });
                        },
                        style: Theme.of(context).textTheme.title,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Title',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                CustomTextField(
                  hintText: '',
                  controller: titleController,
                  inputType: TextInputType.text,
                  obscureText: false,
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Give a description',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                TextField(
                  controller: descriptionController,
                  maxLines: 6,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: '',
                    hintStyle: TextStyle(fontSize: 16),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          width: 2,
                        )),
                    filled: true,
                    contentPadding: EdgeInsets.all(16),
                    fillColor: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      //child: _image1 == null
                      child: img1 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 1;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image1, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 1;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img1,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      child: img2 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 2;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image2, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 2;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img2,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      child: img3 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 3;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image3, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 3;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img3,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      //child: _image4 == null
                      child: img4 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 4;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image4, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 4;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img4,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      //child: _image5 == null
                      child: img5 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 5;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image5, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 5;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img5,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      //child: _image6 == null
                      child: img6 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 6;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image6, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 6;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img6,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      //child: _image7 == null
                      child: img7 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 7;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image7, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 7;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img7,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      //child: _image8 == null
                      child: img8 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 8;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image8, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 8;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img8,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      //child: _image9 == null
                      child: _image9 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 9;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image9, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: (){
                          no = 9;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img9,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      //child: _image10 == null
                      child: img10 == null
                          ? IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                        ),
                        iconSize: 56,
                        color: Colors.black45,
                        onPressed: () {
                          no = 10;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                      )
                      //: Image.file(_image10, width: 60, height: 60, fit: BoxFit.fill,),
                          : GestureDetector(
                        onTap: () {
                          no = 10;

                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                              imagePickerDialog);
                        },
                        child: Image.network(
                          img10,
                          width: 60,
                          height: 60,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Contact Number',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                CustomTextField(
                  hintText: '',
                  controller: contactNoController,
                  inputType: TextInputType.number,
                  obscureText: false,
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Link a Website (optional)',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                CustomTextField(
                  hintText: '',
                  controller: websiteController,
                  inputType: TextInputType.text,
                  obscureText: false,
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Type your current location',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                CustomTextField(
                  hintText: '',
                  controller: locationController,
                  inputType: TextInputType.text,
                  obscureText: false,
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Event Date',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.calendar_today,
                        color: Colors.white,
                      ),
                      iconSize: 56,
                      color: Colors.black45,
                      onPressed: () {
                        _pickDateDialog();
                      },
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Text(
                      _selectedDate != null ? selectedDateValue : '',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),


                SizedBox(height: 16.0,),

                CheckboxListTile(
                  title: Text("Child friendly"),
                  value: childFriendlyValue,
                  onChanged: (newValue) {
                    setState(() {
                      childFriendlyValue = newValue;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                ),

                SizedBox(height: 16.0,),

                CheckboxListTile(
                  title: Text("Parking Value"),
                  value: parkingValue,
                  onChanged: (newValue) {
                    setState(() {
                      parkingValue = newValue;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                ),


                SizedBox(height: 16.0,),

                CheckboxListTile(
                  title: Text("Baby Change Facility"),
                  value: babyChangeFacility,
                  onChanged: (newValue) {
                    setState(() {
                      babyChangeFacility = newValue;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                ),

                SizedBox(height: 16.0,),

                CheckboxListTile(
                  title: Text("Parents room"),
                  value: parentsRoom,
                  onChanged: (newValue) {
                    setState(() {
                      parentsRoom = newValue;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                ),


                SizedBox(
                  height: 16.0,
                ),

                Text(
                  'Family Friendly Features (Optional)',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                TextField(
                  controller: familyFriendlyFeaturesController,
                  maxLines: 6,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: '',
                    hintStyle: TextStyle(fontSize: 16),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          width: 2,
                        )),
                    filled: true,
                    contentPadding: EdgeInsets.all(16),
                    fillColor: Colors.white,
                  ),
                ),



                SizedBox(
                  height: 16.0,
                ),
                Text(
                  '(This post will be deleted 30 days from the event)',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 14.0,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                CustomButton(
                    width: 120.0,
                    height: 50.0,
                    borderColor: Colors.orange,
                    bgColor: Colors.orange,
                    text: 'Post',
                    icon: Icons.send,
                    callback: () => postCallback()),
                SizedBox(
                  height: 16.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }



  Future getImageFromCamera(int no) async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    String timestamp = new DateTime.now().microsecondsSinceEpoch.toString();
    if (image != null) {
      //getCropImage(image, no);
      await getCropImage(image);

      if (no == 1) {
        await uploadPostImageToFirebaseStorage(_image1, timestamp);

        setState(() {
          img1 = postUrl;
        });
      } else if (no == 2) {
        await uploadPostImageToFirebaseStorage(_image2, timestamp);
        setState(() {
          img2 = postUrl;
        });
      } else if (no == 3) {
        await uploadPostImageToFirebaseStorage(_image3, timestamp);

        setState(() {
          img3 = postUrl;
        });
      } else if (no == 4) {
        await uploadPostImageToFirebaseStorage(_image4, timestamp);
        setState(() {
          img4 = postUrl;
        });
      } else if (no == 5) {
        await uploadPostImageToFirebaseStorage(_image5, timestamp);

        setState(() {
          img5 = postUrl;
        });
      } else if (no == 6) {
        await uploadPostImageToFirebaseStorage(_image6, timestamp);

        setState(() {
          img6 = postUrl;
        });
      } else if (no == 7) {
        await uploadPostImageToFirebaseStorage(_image7, timestamp);

        setState(() {
          img7 = postUrl;
        });
      } else if (no == 8) {
        await uploadPostImageToFirebaseStorage(_image8, timestamp);

        setState(() {
          img8 = postUrl;
        });
      } else if (no == 9) {
        await uploadPostImageToFirebaseStorage(_image9, timestamp);
        setState(() {
          img9 = postUrl;
        });
      } else if (no == 10) {
        await uploadPostImageToFirebaseStorage(_image10, timestamp);
        setState(() {
          img10 = postUrl;
        });
      }
    }
  } //end getImageFromCamera()

  //Future getImageFromGallery(int no) async {
  Future getImageFromGallery() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    String timestamp = new DateTime.now().microsecondsSinceEpoch.toString();

    if (image != null) {
      //getCropImage(image, no);
      await getCropImage(image);

      if (no == 1) {
        await uploadPostImageToFirebaseStorage(_image1, timestamp);
        setState(() {
          img1 = postUrl;
        });
      } else if (no == 2) {
        await uploadPostImageToFirebaseStorage(_image2, timestamp);
        setState(() {
          img2 = postUrl;
        });
      } else if (no == 3) {
        await uploadPostImageToFirebaseStorage(_image3, timestamp);
        setState(() {
          img3 = postUrl;
        });
      } else if (no == 4) {
        await uploadPostImageToFirebaseStorage(_image4, timestamp);
        setState(() {
          img4 = postUrl;
        });
      } else if (no == 5) {
        await uploadPostImageToFirebaseStorage(_image5, timestamp);
        setState(() {
          img5 = postUrl;
        });
      } else if (no == 6) {
        await uploadPostImageToFirebaseStorage(_image6, timestamp);
        setState(() {
          img6 = postUrl;
        });
      } else if (no == 7) {
        await uploadPostImageToFirebaseStorage(_image7, timestamp);
        setState(() {
          img7 = postUrl;
        });
      } else if (no == 8) {
        await uploadPostImageToFirebaseStorage(_image8, timestamp);
        setState(() {
          img8 = postUrl;
        });
      } else if (no == 9) {
        await uploadPostImageToFirebaseStorage(_image9, timestamp);
        setState(() {
          img9 = postUrl;
        });
      } else if (no == 10) {
        await uploadPostImageToFirebaseStorage(_image10, timestamp);
        setState(() {
          img10 = postUrl;
        });
      }
    }
  } //end getImageFromGallery()

  //getCropImage(var image, int no) async {
  getCropImage(var image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      setState(() {
        //_image = image;

        //newImage = croppedFile;

        ///prev code
        //_image = croppedFile;

        if (no == 1) {
          _image1 = croppedFile;
        } else if (no == 2) {
          _image2 = croppedFile;
        } else if (no == 3) {
          _image3 = croppedFile;
        } else if (no == 4) {
          _image4 = croppedFile;
        } else if (no == 5) {
          _image5 = croppedFile;
        } else if (no == 6) {
          _image6 = croppedFile;
        } else if (no == 7) {
          _image7 = croppedFile;
        } else if (no == 8) {
          _image8 = croppedFile;
        } else if (no == 9) {
          _image9 = croppedFile;
        } else if (no == 10) {
          _image10 = croppedFile;
        }
      });
    } //end if
  } //getCropImage()

  _permissionRequest() async {
    final permissionValidator = EasyPermissionValidator(
      context: context,
      appName: 'Easy Permission Validator',
    );
    var result = await permissionValidator.camera();
    if (result) {
      // Do something;
    }
  }

  void _pickDateDialog() {
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        //which date will display when user open the picker
        firstDate: DateTime(1950),
        //what will be the previous supported year in picker
        lastDate: DateTime
            .now()) //what will be the up to supported date in picker
        .then((pickedDate) {
      //then usually do the future job
      if (pickedDate == null) {
        //if user tap cancel then this function will stop
        return;
      }
      setState(() {
        //for rebuilding the ui
        _selectedDate = pickedDate;

        selectedDateValue = _selectedDate.year.toString() +
            ' ' +
            _selectedDate.month.toString() +
            ', ' +
            _selectedDate.day.toString();
      });
    });
  }

  uploadPostImageToFirebaseStorage(File image, String timestamp) async {
    String uid = Constants.uidValue;
    print('uid got: $uid');

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference = storage.ref().child("images/$timestamp.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    setState(() {
      postUrl = newUrl;
    });

    int iNo = no;
    iNo--;
    String strImgNo = imageNo.toString();
    //imagesListToUpload.add(new PostImage.fromCode(strImgNo, postUrl));

    print('iNo: $iNo');

    PostImage postImage = new PostImage.fromCode(iNo.toString(), postUrl);
    print('assigned id: ${postImage.id}, assigned image: ${postImage.image}');

    //imagesListToUpload2[iNo] = postImage;
    imagesListToUpload2.removeAt(iNo);
    imagesListToUpload2.insert(iNo, postImage);


  } //end uploadImageToFirebaseStorage()



  assignImages() {

    if (imagesList.length == 1) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
      });
    } else if (imagesList.length == 2) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
      });
    } else if (imagesList.length == 3) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
        img3 = imagesList[2] != null ? imagesList[2] : null;
      });
    } else if (imagesList.length == 4) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
        img3 = imagesList[2] != null ? imagesList[2] : null;
        img4 = imagesList[3] != null ? imagesList[3] : null;
      });
    } else if (imagesList.length == 5) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
        img3 = imagesList[2] != null ? imagesList[2] : null;
        img4 = imagesList[3] != null ? imagesList[3] : null;
        img5 = imagesList[4] != null ? imagesList[4] : null;
      });
    } else if (imagesList.length == 6) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
      });
    } else if (imagesList.length == 7) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
        img3 = imagesList[2] != null ? imagesList[2] : null;
        img4 = imagesList[3] != null ? imagesList[3] : null;
        img5 = imagesList[4] != null ? imagesList[4] : null;
        img6 = imagesList[5] != null ? imagesList[5] : null;
      });
    } else if (imagesList.length == 8) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
        img3 = imagesList[2] != null ? imagesList[2] : null;
        img4 = imagesList[3] != null ? imagesList[3] : null;
        img5 = imagesList[4] != null ? imagesList[4] : null;
        img6 = imagesList[5] != null ? imagesList[5] : null;
        img7 = imagesList[6] != null ? imagesList[6] : null;
        img8 = imagesList[7] != null ? imagesList[7] : null;
      });
    } else if (imagesList.length == 9) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
        img3 = imagesList[2] != null ? imagesList[2] : null;
        img4 = imagesList[3] != null ? imagesList[3] : null;
        img5 = imagesList[4] != null ? imagesList[4] : null;
        img6 = imagesList[5] != null ? imagesList[5] : null;
        img7 = imagesList[6] != null ? imagesList[6] : null;
        img8 = imagesList[7] != null ? imagesList[7] : null;
        img9 = imagesList[8] != null ? imagesList[8] : null;
      });
    } else if (imagesList.length == 10) {
      setState(() {
        img1 = imagesList[0] != null ? imagesList[0] : null;
        img2 = imagesList[1] != null ? imagesList[1] : null;
        img3 = imagesList[2] != null ? imagesList[2] : null;
        img4 = imagesList[3] != null ? imagesList[3] : null;
        img5 = imagesList[4] != null ? imagesList[4] : null;
        img6 = imagesList[5] != null ? imagesList[5] : null;
        img7 = imagesList[6] != null ? imagesList[6] : null;
        img8 = imagesList[7] != null ? imagesList[7] : null;
        img9 = imagesList[8] != null ? imagesList[8] : null;
        img10 = imagesList[9] != null ? imagesList[9] : null;
      });
    }
  }

  postCallback() {

    var databaseReference;
    print('post Callback');

    String dt = _selectedDate == null
        ? selectedDateValue
        : _selectedDate.microsecondsSinceEpoch.toString();

    try {
      databaseReference =
          FirebaseDatabase.instance.reference().child('AllPost');
      databaseReference.child(widget.postData['date']).update({
        'category': widget.postData['category'],
        'contactNo': contactNoController.text,
        'date': dt,
        'description': descriptionController.text,
        'linkOfWeb': websiteController.text,
        'location': locationController.text,
        'title': titleController.text,
        'childFriendly':childFriendlyValue,
        'parkingValue':parkingValue,
        'babyChangeFamily':babyChangeFacility,
        'parentsRoom':parentsRoom,
        'familyFriendsFeatures':familyFriendlyFeaturesController.text,
      });
    }catch(e) {
      print(e);
    }


    //if(imagesListToUpload.length > imagesList.length) {

    print('imagesListToUpload length: ${imagesListToUpload2.length}');
    for (var item in imagesListToUpload2) {

      if(item != null) {
        databaseReference.child(originalDate).child("images").update({
          item.id: item.image,
        })
            .then((value) => (){

          Fluttertoast.showToast(
              msg: 'Post uploaded successfuly !',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 3,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);


        });

      }


    }//end for

    //}//end if



  }

  createImagesListToUpload() {

    for(int i=0; i<imagesList.length; i++) {
      //imageNo++;
      //imagesListToUpload.add(new PostImage.fromCode(imageNo.toString(), imagesList[i]));
      imagesListToUpload2.removeAt(i);
      imagesListToUpload2.insert(i, new PostImage.fromCode(i.toString(), imagesList[i]));

    }

  }

}
