import 'dart:async';
import 'package:akidemic_life/pages/home_page.dart';
import 'package:akidemic_life/pages/signup_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/utils.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:akidemic_life/widgets/custom_text_field.dart';
import 'package:akidemic_life/widgets/login_logo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:akidemic_life/models/user_account_info.dart';
import 'no_email_verification_page.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final _auth = FirebaseAuth.instance;
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllUsers');

  TextEditingController emailController;
  TextEditingController passwordController;

  String email, password, fullName, phoneNo, location, instituteName;

  //form validation
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _autoValidate;
  bool isLoading;
  bool switchStatus;

  @override
  void initState() {
    super.initState();

    emailController = TextEditingController();
    passwordController = TextEditingController();

    /*getRememberMeStatus().then((value) => (){

    });*/

    _autoValidate = false;
    isLoading = false;
    switchStatus = false;

    /*databaseReference.once().then((value) => (DataSnapshot snapshot){

      var KEYS = snapshot.value.keys;
      var DATA = snapshot.value;

      for(var individualKey in KEYS) {

        UserData userData = UserData(
          fullName: DATA[individualKey]['fullName'],
          phoneNumber: DATA[individualKey]['phoneNumber']
        );

        list.add(userData);

        setState(() {
          print('length: ${list.length}');
        });
      }

    });*/


    checkRememberMeStatus().then((value) {
      if (value) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(
              title: Constants.appName,
            ),
          ),
        );
      }
    });


  }//end initState()

  Future<bool> checkRememberMeStatus() async {
    bool status = await Utils.getRememberMeStatus();

    return status == null ? false : status;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: _body(),
      //body: loading(),
    );
  }

  SafeArea _body() {
    return SafeArea(
      child: SingleChildScrollView(
          child: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              autovalidate: _autoValidate,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 24.0,
                  ),
                  LoginLogo(),
                  SizedBox(
                    height: 24.0,
                  ),
                  Text(
                    'Login Page',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 26.0,
                    ),
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  CustomTextField(
                    hintText: 'Email',
                    controller: emailController,
                    inputType: TextInputType.text,
                    obscureText: false,
                    onChange: (value) {
                      email = value;
                    },
                    validator: (String arg) {
                      if (arg == null || arg == '') {
                        return 'email cannot be empty';
                      } else
                        return null;
                    },
                    onSaved: (String val) {
                      email = val;

                      Constants.userEmail = email;

                    },
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  CustomTextField(
                    hintText: 'Password',
                    controller: passwordController,
                    inputType: TextInputType.visiblePassword,
                    obscureText: true,
                    onChange: (value) {
                      password = value;
                    },
                    validator: (String arg) {
                      if (arg == null || arg == '') {
                        return 'password cannot be empty';
                      } else
                        return null;
                    },
                    onSaved: (String val) {
                      password = val;
                    },
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        'Save user login details',
                        style: TextStyle(color: Colors.black45, fontSize: 18),
                      ),
                      Switch(
                        value: switchStatus,
                        onChanged: (value) {
                          setState(() {
                            switchStatus = value;

                            Utils.rememberUser(switchStatus);

                          });
                        },
                        activeTrackColor: Colors.lightGreenAccent,
                        activeColor: Colors.green,
                      ),
                    ],
                  ),
                  CustomButton(
                    width: 140.0,
                    height: 50.0,
                    borderColor: Color(Constants.buttonColor),
                    bgColor: Color(Constants.buttonColor),
                    text: 'Login',
                    icon: Icons.send,
                    callback: () => loginCallback(),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  Text(
                    'not a member? signup now',
                    style: TextStyle(color: Colors.deepPurple, fontSize: 18.0),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  CustomButton(
                      width: 140.0,
                      height: 50.0,
                      borderColor:Color(Constants.buttonColor),
                      bgColor: Color(Constants.buttonColor),
                      text: 'Join us',
                      icon: Icons.send,
                      callback: () => registerCallback()),
                ],
              ),
            ),
          ),
          Visibility(
            visible: isLoading,
            child: Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              ),
            ),
          ),
        ],
      )),
    );
  }

  loginCallback() {

    _validateInputs();
    //authenticateUser();

  }

  authenticateUser() async {
    try {
      final auth = await _auth.signInWithEmailAndPassword(
          email: email, password: password);

      if (auth != null) {
        setState(() {
          isLoading = false;
        });

        Constants.uidValue = auth.user.uid;
        //Utils.saveUIDToSF(Constants.uidValue, Constants.uidValue);  //delete this later
        Utils.saveUIDToSF('uid', Constants.uidValue);
        Utils.rememberUser(switchStatus);


        //old code
        /*Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(
              title: 'AKIDemic Life',
            ),
          ),
        );*/
/*
        var user = FirebaseAuth.instance.currentUser;
        if (user.emailVerified) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => HomePage(
                title: 'AKIDemic Life',
              ),
            ),
          );
        } else {



          UserData userData = await getUserInfoFromFirebaseDb();

          *//*getUserInfoFromFirebaseDb().then(
            (value) => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => NoEmailVerificationPage(
                    name: value.name, phoneNo: value.phoneNo, newUser: user),
              ),
            ),
          );*//*



          //getUserInfoFromFirebaseDb();

          print('received name ${userData.name} phoneNo ${userData.phoneNo}');
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => NoEmailVerificationPage(
                  name: userData.name, phoneNo: userData.phoneNo, newUser: user),
            ),
          );



          *//*Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => NoEmailVerificationPage(
                  name: fullName, phoneNo: phoneNo, newUser: user),
            ),
          );*//*

        }*/
      }
    } catch (error) {
      //print('error ${error.message}');

      setState(() {
        isLoading = false;
      });

      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  } //end authenticateUser()

  Future<String> signIn(String email, String password) async {
    User user;
    String errorMessage;

    try {
      final auth = await _auth.signInWithEmailAndPassword(
          email: email, password: password);

      user = auth.user;
    } catch (error) {
      switch (error.code) {
        case "ERROR_INVALID_EMAIL":
          errorMessage = "Your email address appears to be malformed.";
          break;
        case "ERROR_WRONG_PASSWORD":
          errorMessage = "Your password is wrong.";
          break;
        case "ERROR_USER_NOT_FOUND":
          errorMessage = "User with this email doesn't exist.";
          break;
        case "ERROR_USER_DISABLED":
          errorMessage = "User with this email has been disabled.";
          break;
        case "ERROR_TOO_MANY_REQUESTS":
          errorMessage = "Too many requests. Try again later.";
          break;
        case "ERROR_OPERATION_NOT_ALLOWED":
          errorMessage = "Signing in with Email and Password is not enabled.";
          break;
        default:
          errorMessage = "An undefined Error happened.";
      }
    }

    if (errorMessage != null) {
      return Future.error(errorMessage);
    }

    return user.uid;
  }

  void _validateInputs() async {

    if (_formKey.currentState.validate()) {
      //    If all data are correct then save data to out variables
      _formKey.currentState.save();

      setState(() {
        isLoading = true;
      });

      //authenticateUser();
      await authenticateUser();

      emailVerificationCheck();

    } else {
      //    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  emailVerificationCheck() async {

    UserAccountInfo userInfo = await getUserInfo();

    print('uid at login: ${userInfo.uid}');

    Utils.saveEmailInSP(userInfo.uid, emailController.text);
    Utils.saveFullNameInSP(userInfo.fullName);
    Utils.savePhoneNoInSP(userInfo.phoneNumber);
    Utils.saveUIDToSF(Constants.uid, userInfo.uid);

    //Constants.userEmail = emailController.text;

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => HomePage(
          title: 'AKIDemic Life',
        ),
      ),
    );



    //temprarily commented
    /*
    var user = FirebaseAuth.instance.currentUser;

    if (user.emailVerified) {

      Fluttertoast.showToast(
          msg: 'Login successful',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);

      Utils.saveFullNameInSP(userInfo.fullName);
      Utils.savePhoneNoInSP(userInfo.phoneNumber);

      print('sending name: ${userInfo.fullName}');

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => HomePage(
            title: 'AKIDemic Life',
          ),
        ),
      );
    } else {




      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => NoEmailVerificationPage(
              name: userInfo.fullName, phoneNo: userInfo.phoneNumber, newUser: user),
        ),
      );


    }
    */


  }


  Future<UserAccountInfo> getUserInfo() async {

    print('uid value ${Constants.uidValue}');

    Completer<UserAccountInfo> completer = new Completer<UserAccountInfo>();

    FirebaseDatabase.instance
        .reference()
        .child("AllUsers")
        .child(Constants.uidValue)
        .once()
        .then((DataSnapshot snapshot) {
      var userInfo = new UserAccountInfo.fromJson(snapshot.key, snapshot.value);
      completer.complete(userInfo);
    });

    return completer.future;
  }
/*

  Future<bool> getRememberMeStatus() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool(Constants.rememberUser);

    print('login status: $boolValue');

    if(boolValue) {

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => HomePage(
            title: Constants.appName,
          ),
        ),
      );

    }

    return boolValue;
  }*/


  registerCallback() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => SignupPage(),
      ),
    );
  }



}
