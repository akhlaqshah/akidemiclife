import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:akidemic_life/models/NewPost.dart';
import 'package:akidemic_life/models/PlacesResult.dart';
import 'package:akidemic_life/pages/post_details.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/globals.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'about_us_page.dart';
import 'google_places_api_demo.dart';

class SearchVillage extends StatefulWidget {
  @override
  _SearchVillageState createState() => _SearchVillageState();
}

class _SearchVillageState extends State<SearchVillage> {
  final locationController = TextEditingController();
  String dropdownValue = eventsList[0];
  final dbRef = FirebaseDatabase.instance.reference().child("AllPost");
  FirebaseStorage storage = FirebaseStorage.instance;

  List<NewPost> postList = List<NewPost>();
  List<String> sortType = ['Newest Post', 'Oldest Post'];
  String _selectedSortType;

  bool loading = false;
  String url;
  bool isAdmin = false;
  File _image;
  int resultsFound = 0;
  bool reverseList = true;

  @override
  void initState() {
    if (Constants.userEmail.contains('admin123@gmail.com')) {
      isAdmin = true;
    }

    displayBannerAImage();
    super.initState();
    _selectedSortType = sortType[0];
  }

  @override
  Widget build(BuildContext context) {
    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: true,
        backgroundColor: Colors.blue,
        body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          child: Column(
            children: [
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        "Find Your Village",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: url == null
                          ? CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            )
                          : CachedNetworkImage(
                              imageUrl: url,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  Image.asset('assets/images/placeholder.png'),
                              width: double.infinity,
                              height: 120,
                              fit: BoxFit.cover,
                            ),
                    ),
                    Visibility(
                        visible: isAdmin ? true : false,
                        child: Align(
                          alignment: Alignment.center,
                          child: FlatButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      imagePickerDialog);
                            },
                            child: Text(
                              "Change Photo",
                              style: TextStyle(fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Text(
                        "Search location",
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    TextField(
                      controller: locationController,
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: 'Search location',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide:
                                BorderSide(color: Colors.black, width: 2.0)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide:
                                BorderSide(color: Colors.black, width: 2.0)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide:
                                BorderSide(color: Colors.black, width: 2.0)),
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(top: 16, right: 8),
                        prefixIcon: InkWell(
                          onTap: () async {
                            PlacesResult placesResult = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => GooglePlacesAPIDemo(),
                              ),
                            );

                            setState(() {
                              locationController.text = placesResult.address;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Transform.rotate(
                              angle: 45 * pi / 180,
                              child: Icon(
                                Icons.navigation,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6.0),
                      child: Text(
                        "Search for service",
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.black,
                          width: 2.0,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: DropdownButton<String>(
                          value: dropdownValue,
                          icon: Padding(
                            padding: const EdgeInsets.only(left: 50.0),
                            child: Icon(Icons.arrow_downward),
                          ),
                          iconSize: 24,
                          elevation: 16,
                          style: TextStyle(
                              color: Colors.deepPurple,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2,
                              wordSpacing: 3,
                              fontSize: 18.0),
                          onChanged: (String newValue) {
                            setState(() {
                              dropdownValue = newValue;
                            });
                          },
                          items: eventsList
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("Sort by:"),
                        DropdownButton<String>(
                          value: _selectedSortType,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 8,
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 16.0),
                          onChanged: (String newValue) {
                            setState(() {
                              _selectedSortType = newValue;
                              switch (newValue) {
                                case 'Newest Post':
                                  reverseList = true;
                                  break;
                                case 'Oldest Post':
                                  reverseList = false;
                                  break;
                              }
                            });
                          },
                          items: sortType
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                        RaisedButton(
                          onPressed: () {
                            if (locationController.text.isNotEmpty) {
                              searchPost(
                                  locationController.text, dropdownValue);
                            } else {
                              Fluttertoast.showToast(
                                  msg: "Location cannot be empty");
                            }
                            setState(() {
                              loading = true;
                            });
                          },
                          color: Colors.orangeAccent,
                          elevation: 8.0,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "Search",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              side: BorderSide(color: Colors.orange)),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("We found $resultsFound results"),
                    ),
                  ],
                ),
              ),
              loading
                  ? Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                    )
                  : Expanded(
                      child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: postList.length,
                          reverse: reverseList,
                          itemBuilder: (context, index) {
                            String imageUrl =
                                'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg';

                            if (postList[index].images != null) {
                              imageUrl = postList[index].images[0] ??
                                  postList[index].images[
                                      postList[index].images.length - 1];
                            }

                            print('image url init $imageUrl');
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => PostDetails(
                                                post: postList[index],
                                              )));
                                },
                                child: Material(
                                  elevation: 10,
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            postList[index]
                                                    .title
                                                    .toUpperCase() ??
                                                " No Title",
                                            style: TextStyle(
                                                color: Colors.blue,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          CachedNetworkImage(
                                            imageUrl: imageUrl,
                                            placeholder: (context, url) =>
                                                Container(
                                                    width: 50,
                                                    height: 50,
                                                    child:
                                                        CircularProgressIndicator()),
                                            errorWidget: (context, url,
                                                    error) =>
                                                Image.asset(
                                                    'assets/images/placeholder.png'),
                                            width: double.infinity,
                                            height: 200,
                                            fit: BoxFit.cover,
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(postList[index].location),
                                        ]),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  searchPost(String searchKey, String category) async {
    final dataBase = FirebaseDatabase.instance.reference().child("AllPost");
    postList.clear();

    await dataBase.once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> values = snapshot.value;
      values.forEach((key, values) {
        // here insert all the user into a list
        print('firebase database values ' +
            values['location'].toString().toLowerCase() +
            ' and category ' +
            values['category'].toString().toLowerCase() +
            ' and $category');

        if (values['location']
                .toString()
                .toLowerCase()
                .contains(searchKey.toLowerCase()) &&
            values['category']
                .toString()
                .toLowerCase()
                .contains(category.toLowerCase())  && values['status'].toString().startsWith('approv') ) {
          print('values from firebase database: ' +
                  values["location"] +
                  ' images ' +
                  values['images'].toString() ??
              " null ");
          setState(() {
            postList.add(NewPost(
                values['category'],
                values['contactNo'],
                values['date'],
                values['description'],
                values['id'],
                values['linkOfWeb'],
                values['location'],
                values['title'],
                values['images']?.cast<String>(),
                values['childFriendly'],
                values['parkingValue'],
                values['babyChangeFacility'],
                values['parentsRoom'],
                values['status'],
                values['review'],
                values['rating'],
                values['familyFriendsFeatures']));

            setState(() {
              loading = false;
              resultsFound = postList.length;
            });
          });
        }
      });
    });

    if (loading) {
      setState(() {
        loading = false;
        resultsFound = postList.length;
      });
    }
  }

  uploadImageToFirebaseStorage(File image) async {
    final databaseReference =
        FirebaseDatabase.instance.reference().child('bannerAImages');

    String uid = Constants.uidValue;
    print('uid got: $uid');

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference =
        storage.ref().child("bannerimages/find_your_village_$uid.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    setState(() {
      url = newUrl;
    });

    databaseReference.child(uid).set({'id': uid, 'value': url});
  } //end uploadImageToFirebaseStorage()

  Future getImageFromCamera() async {
    //ImagePicker imagePicker = ImagePicker();
    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image != null) {
      await getCropImage(image);

      await uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromCamera()

  Future getImageFromGallery() async {
    //ImagePicker imagePicker = ImagePicker();

    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      await getCropImage(image);

      await uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromGallery()

  getCropImage(var image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      setState(() {
        //_image = image;
        _image = croppedFile;
        //newImage = croppedFile;
      });
    } //end if
  } //getCropImage()

  void displayBannerAImage() async {
    BannerAImage bannerAImage = await getBannerAImage();

    setState(() {
      url = bannerAImage.value;
      print('banner image: $url');
    });
  }

  Future<BannerAImage> getBannerAImage() async {
    Completer<BannerAImage> completer = new Completer<BannerAImage>();

    var dbRef = FirebaseDatabase.instance.reference().child("bannerAImages");
    dbRef
        .orderByKey()
        .equalTo(Constants.adminKey)
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot.value != null) {
        if (snapshot.value.isNotEmpty) {
          FirebaseDatabase.instance
              .reference()
              .child('bannerAImages')
              .child(Constants.adminKey)
              .once()
              .then((DataSnapshot snapshot) {
            var bannerAImage =
                new BannerAImage.fromJson(snapshot.key, snapshot.value);
            completer.complete(bannerAImage);
          });
        }
      }
    });

    return completer.future;
  }
}
