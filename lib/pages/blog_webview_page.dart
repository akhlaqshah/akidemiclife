import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';


class BlogWebviewPage extends StatefulWidget {

  String imageUrl;
  String blogAddress;

  BlogWebviewPage({this.imageUrl, this.blogAddress});

  @override
  _BlogWebviewPageState createState() => _BlogWebviewPageState();
}

class _BlogWebviewPageState extends State<BlogWebviewPage> {

  final _key = UniqueKey();

  List<WebViewController> _listController = List();



  static final List<String> htmlStrings = [
    '''<p>Table shows some compounds and the name of their manufacturing process</p>
<table style="border-collapse: collapse; width: 100%; height: 85px;" border="1">
<tbody>
<tr style="height: 17px;">
<td style="width: 50%; text-align: center; height: 17px;">Compounds/Elements</td>
<td style="width: 50%; text-align: center; height: 17px;">Manufacture process</td>
</tr>
<tr style="height: 17px;">
<td style="width: 50%; height: 17px;">Nitric acid</td>
<td style="width: 50%; height: 17px;">Ostwald's process</td>
</tr>
<tr style="height: 17px;">
<td style="width: 50%; height: 17px;">Ammonia</td>
<td style="width: 50%; height: 17px;">Haber's process</td>
</tr>
<tr style="height: 17px;">
<td style="width: 50%; height: 17px;">Sulphuric acid</td>
<td style="width: 50%; height: 17px;">Contact process</td>
</tr>
<tr style="height: 17px;">
<td style="width: 50%; height: 17px;">Sodium</td>
<td style="width: 50%; height: 17px;">Down's process</td>
</tr>
</tbody>
</table>''',
    '''<p>\(L=[M{L }^{2 }{T }^{-2 }{A }^{-2 }]\)</p>
<p>\(C=[{M }^{-1 }{L }^{-2 }{T }^{4 }{A }^{2 }]\)</p>
<p>\(R=[M{L }^{2 }{T }^{-3 }{A }^{-2 }]\)</p>
<p>\(\therefore \frac {R}{L}=\frac{[M{L }^{2 }{T }^{-2 }{A }^{-2 }]}{[M{L }^{2 }{T }^{-3 }{A }^{-2 }]}=[T]\)</p>''',
    '''<p>Displacement(s)\(=\left(13.8\pm0.2\right)m\)</p>
<p>Time(t)\(=\left(4.0\pm0.3\right)s\)</p>
<p>Velocity(v)\(=\frac{13.8}{4.0}=3.45m{s}^{-1}\)</p>
<p>\(\frac{\delta v}{v}=\pm\left(\frac{\delta s}{s}+\frac{\delta t}{t}\right)=\pm\left(\frac{0.2}{13.8}+\frac{0.3}{4.0}\right)=\pm0.0895\)</p>
<p>\(\delta v =\pm0.0895*3.45=\pm0.3\)(rounding off to one place of decimal)</p>
<p>\(v=\left(3.45\pm0.3\right)m{s}^{-1}\)</p>''',
    '''<p>The only real numbers satisfying \(x^2=4\) are 2 and -2. But none of them satisfy the final condition, \(x+2=3\). So, there is no real number such that these conditions are met. Hence this is null set.</p>
<p>Note that, for \(x\) to be a memner of \(\{x:p(x),q(x)\}\),&nbsp;<em><strong>both</strong></em> \(p(x)\) and \(q(x)\) should be true.</p>''',
  ];



  List<double> _heights =
  List<double>.generate(htmlStrings.length, (int index) => 20.0);










  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: <Widget>[

              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 4,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage('${widget.imageUrl}'),
                  ),
                ),
              ),


              ///webview
              /*Container(
                height: 800.0,
                child: WebView(
                    key: _key,

                  onPageFinished: (some) async {
                    double height = double.parse(await _listController[index]
                        .evaluateJavascript(
                        "document.documentElement.scrollHeight;"));
                    setState(() {
                      _heights[index] = height;
                    });
                  },

                    javascriptMode: JavascriptMode.unrestricted,
                    initialUrl: widget.blogAddress,),
              ),*/

              ///webview 2

        ListView.builder(
          primary: false,
          shrinkWrap: true,
          itemCount: 1,
          itemBuilder: (context, index) {
            return Container(
              height: _heights[index] + 300 ?? 100.0,
              child: WebView(
                initialUrl:
                //'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(htmlStrings[index]))}',
                '${widget.blogAddress}',
                onPageFinished: (some) async {
                  double height = double.parse(await _listController[index]
                      .evaluateJavascript(
                      "document.documentElement.scrollHeight;"));
                  setState(() {
                    _heights[index] = height;
                  });
                },
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (controller) async {
                  _listController.add(controller);
                },
              ),
            );
          },
        ),


            ],
          ),
        ),
      ),

    );
  }
}
