import 'package:akidemic_life/models/NewPost.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class PostReviewPage extends StatefulWidget {
  final NewPost post;

  PostReviewPage({this.post});

  @override
  _PostReviewPageState createState() => _PostReviewPageState();
}

class _PostReviewPageState extends State<PostReviewPage> {
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllPost');
  TextEditingController commentController;
  double newRating;
  bool noImageAvailable = false;
  String imageUrl =
      'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg';

  @override
  void initState() {
    super.initState();

    commentController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {

    if (widget.post.images == null) {
      noImageAvailable = true;
    } else {
      imageUrl = widget.post.images[0] ?? widget.post.images[1];
    }
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Location: ${widget.post.location}'),
                SizedBox(
                  width: 16.0,
                ),
             
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    Text('Service: '),
                    SizedBox(
                      width: 16.0,
                    ),
                    Text(widget.post.category),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    Text('Title: '),
                    SizedBox(
                      width: 16.0,
                    ),
                    Text(widget.post.title),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    Text('Description: '),
                    SizedBox(
                      width: 16.0,
                    ),
                    Expanded(child: Text(widget.post.description)),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Image.network(imageUrl),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: RatingBar(
                    initialRating: 0,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);

                      setState(() {
                        newRating = rating;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  'Comment',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                TextField(
                  controller: commentController,
                  maxLines: 6,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: '',
                    hintStyle: TextStyle(fontSize: 16),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          width: 2,
                        )),
                    filled: true,
                    contentPadding: EdgeInsets.all(16),
                    fillColor: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                CustomButton(
                    width: 140.0,
                    height: 50.0,
                    borderColor: Color(Constants.buttonColor),
                    bgColor: Color(Constants.buttonColor),
                    text: 'submit'.toUpperCase(),
                    icon: Icons.send,
                    callback: () => submitCallback()),
              ],
            ),
          ),
        ),
      ),
    );
  }

  submitCallback() async {
    await FirebaseFirestore.instance
        .collection('reviews/' + widget.post.date + '/postreviews')
        .doc(Constants.userEmail)
        .set({
      'user': Constants.usernameValue,
      'comment': commentController.text,
      'rating': newRating,
      'timestamp': Timestamp.now(),
    });
    //add review and rating to fireabse database
    /*databaseReference.child(widget.post.update({

      */ /*'category':widget.post['category'],
      'contactNo':widget.post['contactNo'],
      'date':widget.post['date'],
      'description':widget.post['description'],
      'linkOfWeb':widget.post['linkOfWeb'],
      'location':widget.post['location'],
      'title':widget.post['title'],
      */ /*
      'rating':newRating.toString(),
      'review':commentController.text,

    });*/

    Navigator.pop(context);
  }
}
