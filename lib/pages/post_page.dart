import 'package:akidemic_life/pages/edit_post_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/utils.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

class PostPage extends StatefulWidget {
  Map postData;

  PostPage({this.postData});

  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllPost');
  final deleteDatabaseReference =
      FirebaseDatabase.instance.reference().child('deleteQueue');
  bool isAdmin = false;

  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  int _current = 0;

  TextEditingController descriptionController;
  final _requestController = TextEditingController();
  String desc;
  bool noImageAvailable = false;
  bool singleImage = false;
  String imageUrl =
      'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg';

  List imagesList = [];

  @override
  void initState() {
    super.initState();

    desc = 'test description';

    descriptionController =
        TextEditingController(text: widget.postData['description']);

    imagesList = widget.postData['images'];
    if (Constants.userEmail.contains('admin123@gmail.com')) {
      isAdmin = true;
    }

    print('images length: ${imagesList?.length}' +
        ' and date ' +
        widget.postData['date']);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.postData['images'] != null){
      if (imagesList.length ==1) {
        singleImage = true;

        imageUrl = imagesList[0] ?? imagesList[1];

        print('imagesUrl init : $imageUrl and $singleImage');
      }else{
        imageUrl = imagesList[0];
      }
    } else {
      noImageAvailable = true;
    }
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: Center(
        child: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Center(
                    child: Text(
                      widget.postData['title'],
                      style: TextStyle(color: Colors.black, fontSize: 20.0),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    margin: EdgeInsets.all(16.0),
                    child: noImageAvailable
                        ? Center(
                            child: Image.network(
                              'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg',
                              height: 200,
                            ),
                          )
                        : singleImage
                            ? Center(
                                child: Image.network(
                                  imageUrl ??
                                      'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg',
                                  height: 200,
                                ),
                              )
                            : Stack(
                                alignment: Alignment.bottomRight,
                                children: <Widget>[
                                  CarouselSlider(
                                    viewportFraction: 1.0,
                                    aspectRatio: 2.0,
                                    autoPlay: false,
                                    reverse: true,
                                    initialPage: 1,
                                    enlargeCenterPage: false,
                                    items: map<Widget>(
                                      //imgList,
                                      imagesList,
                                      (index, i) {
                                        return Container(
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: NetworkImage(imagesList[
                                                            index] ??
                                                        'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg'),
                                                fit: BoxFit.cover),
                                          ),
                                        );
                                      },
                                    ),
                                    onPageChanged: (index) {
                                      setState(() {
                                        _current = index;
                                      });
                                    },
                                  ),
                                  indicator(_current),
                                ],
                              ),
                  ),
                  TextField(
                    controller: descriptionController,
                    maxLines: 6,
                    readOnly: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: '',
                      hintStyle: TextStyle(fontSize: 16),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: BorderSide(width: 2, color: Colors.black),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: BorderSide(width: 2, color: Colors.black),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: BorderSide(width: 2, color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          borderSide: BorderSide(
                            width: 2,
                          )),
                      filled: true,
                      contentPadding: EdgeInsets.all(16),
                      fillColor: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  InkWell(
                    onTap: () {
                      ///launch url in web browser
                      Utils.launchURL(widget.postData['linkOfWeb']);
                    },
                    child: Text(
                      'Click here to visit website',
                      style: TextStyle(
                        color: Colors.deepPurple,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        'Contact: ',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        widget.postData['contactNo'],
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  Text(
                    'Address: ' + widget.postData['location'],
                    style: TextStyle(
                      fontSize: 14.0,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CustomButton(
                          width: 140.0,
                          height: 50.0,
                          borderColor: Colors.red,
                          bgColor: Colors.red,
                          text: 'Delete'.toUpperCase(),
                          icon: Icons.send,
                          callback: () =>
                              isAdmin ? adminDelete() : showDialogue()),
                      CustomButton(
                          width: 140.0,
                          height: 50.0,
                          borderColor: Colors.green,
                          bgColor: Colors.green,
                          text: 'Edit'.toUpperCase(),
                          icon: Icons.send,
                          callback: () => editPostAction()),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  showDialogue() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text('Reason of deleting post'),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextFormField(
                  controller: _requestController,
                  maxLines: 3,
                  decoration: InputDecoration(
                    labelText: "Enter reason here",
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  keyboardType: TextInputType.text,
                ),
              ],
            ),
            actions: <Widget>[
              RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Color(Constants.buttonColor),
                  textColor: Colors.white,
                  child: Text(
                    "Send",
                  ),
                  onPressed: () async {
                    if (_requestController.text.isNotEmpty) {
                      deletePostAction(_requestController.text);
                      Navigator.pop(context);
                    } else {
                      Fluttertoast.showToast(
                          msg: "Enter the reason to delete the post");
                    }
                  }),
            ],
          );
        });
  }

  deletePostAction(String reason) {
    //delete post action

    String dt = widget.postData['date'];
    print('date of deletion $dt');

    deleteDatabaseReference.child(widget.postData['date']).set({

      'reason': reason,
      'images': widget.postData['images'],
      'id': widget.postData['id'],
      'category': widget.postData['category'],
      'date': widget.postData['date'],
      'title': widget.postData['title'],
      'description': widget.postData['description'],
      'familyFriendsFeatures': widget.postData['familyFriendsFeatures'],
      'linkOfWeb': widget.postData['linkOfWeb'],
      'parentsRoom': widget.postData['parentsRoom'],
      'contactNo': widget.postData['contactNo'],
      'parkingValue': widget.postData['parkingValue'],
      'status': widget.postData['status'],
      'childFriendly': widget.postData['childFriendly'],
      'babyChangeFamily': widget.postData['babyChangeFamily'],
      'location': widget.postData['location']
    });



    databaseReference.child(widget.postData['date']).remove().then((_) {
      Navigator.pop(context);
    });
    Fluttertoast.showToast(msg: "Delete request sent to admin");
    Navigator.pop(context);


    // databaseReference.child(widget.postData['date']).remove().then((_) {
    //   Navigator.pop(context, 1);
    // });
  }

  adminDelete() {
    print('admin deleted post');
    Fluttertoast.showToast(msg: "Post deleted successfully");
    databaseReference.child(widget.postData['date']).remove().then((_) {
      Navigator.pop(context);
    });
  }

  editPostAction() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditPostPage(
          postData: widget.postData,
        ),
      ),
    );
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  Padding indicator(int _current) {
    return Padding(
      padding: const EdgeInsets.only(right: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: map<Widget>(
          //imgList,
          imagesList,
          (index, url) {
            return Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Color.fromRGBO(0, 0, 0, 0.9)
                      : Color.fromRGBO(0, 0, 0, 0.4)),
            );
          },
        ),
      ),
    );
  }
}
