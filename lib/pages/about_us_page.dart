import 'dart:async';
import 'dart:io';

import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {

  FirebaseStorage storage = FirebaseStorage.instance;

  File _image;
  String url;

  @override
  void initState() {
    super.initState();
    _permissionRequest();

    displayBannerAImage();
  }

  @override
  Widget build(BuildContext context) {

    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Our Story',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),

                /*Center(
                  child: _image == null
                      ? Image.network(
                          'https://i.ibb.co/z5g98qc/water-wallpaper.jpg')
                      : Image.file(_image),
                ),*/

                 Align(
                  alignment: Alignment.center,
                  child: url == null
                      ? Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                      : CachedNetworkImage(
                          imageUrl: url,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Image.asset('assets/images/placeholder.png'),
                          width: double.infinity,
                          height: 150,
                          fit: BoxFit.cover,
                        ),
                ),


                SizedBox(
                  height: 8.0,
                ),



                Center(
                  child: Constants.userEmail == 'admin123@gmail.com' ? FlatButton(
                    color: Color(0xFF0E82C6),
                    textColor: Colors.white,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => imagePickerDialog);
                    },
                    child: Text(
                      'Change Photo',
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black,
                      ),
                    ),
                  ) : Container(),
                ),


                SizedBox(
                  height: 16.0,
                ),

                Text(
                  'The story & diverse international team behind AKIDemic Life',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'PlayfairDisplay',
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Who we are',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Container(
                  height: 220.0,
                  padding: EdgeInsets.all(12.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                    child: Text(Constants.whoWeAre),
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'About the app developers',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Container(
                  height: 150.0,
                  padding: EdgeInsets.all(12.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                    child: Text(Constants.aboutTheDevelopers),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  uploadImageToFirebaseStorage(File image) async {

    final databaseReference =
    FirebaseDatabase.instance.reference().child('AboutPic');

    String uid = Constants.uidValue;
    print('uid got: $uid');

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference = storage.ref().child("bannerimages/about_pic$uid.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    setState(()  {
      url = newUrl;
    });

    databaseReference.child(uid).set({'id': uid, 'value': url});
  } //end uploadImageToFirebaseStorage()


  void displayBannerAImage() async {

    print('inside display about pic');
    BannerAImage bannerAImage = await getBannerAImage();

    print('about url value: ${bannerAImage.value}');

    setState(() {
      url = bannerAImage.value;
      print('about pic: $url');
    });



  }

  Future<BannerAImage> getBannerAImage() async {

    Constants.uidValue = await Utils.getUIDFromSF();
    String key = Constants.userEmail == 'admin123@gmail.com' ? Constants.uidValue : Constants.adminKey;

    Completer<BannerAImage> completer = new Completer<BannerAImage>();

    var dbRef = FirebaseDatabase.instance.reference().child("AboutPic");

    //dbRef.orderByKey().equalTo(Constants.uidValue).once().then((DataSnapshot snapshot) {
    dbRef.orderByKey().equalTo(key).once().then((DataSnapshot snapshot) {
      if (snapshot.value != null) {
        if (snapshot.value.isNotEmpty) {

          FirebaseDatabase.instance
              .reference()
              .child('AboutPic')
              //.child(Constants.uidValue)
              .child(key)
              .once()
              .then((DataSnapshot snapshot) {

            var bannerAImage = new BannerAImage.fromJson(snapshot.key, snapshot.value);
            completer.complete(bannerAImage);
          });

        }
      }
    });


    return completer.future;

  }





  Future getImageFromCamera() async {
    //ImagePicker imagePicker = ImagePicker();
    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image != null) {
      await getCropImage(image);

      await uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromCamera()

  Future getImageFromGallery() async {
    //ImagePicker imagePicker = ImagePicker();

    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      //getCropImage(image, no);
      await getCropImage(image);

      await uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromGallery()

  getCropImage(var image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      setState(() {
        ///prev code
        _image = croppedFile;
      });
    } //end if
  } //getCropImage()

  _permissionRequest() async {
    final permissionValidator = EasyPermissionValidator(
      context: context,
      appName: 'Easy Permission Validator',
    );
    var result = await permissionValidator.camera();
    if (result) {
      // Do something;
    }
  }



}


class BannerAImage {

  final String key;
  String id;
  String value;

  BannerAImage.fromJson(this.key, Map data) {

    id = data['id'];
    if (id == null) {
      id = '';
    }

    value = data['value'];
    if (value == null) {
      value = '';
    }

  }

}
