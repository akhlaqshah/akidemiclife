import 'dart:async';
import 'dart:io';

import 'package:akidemic_life/models/PlacesResult.dart';
import 'package:akidemic_life/models/post_image.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:akidemic_life/utils/globals.dart';
import 'package:akidemic_life/widgets/custom_button.dart';
import 'package:akidemic_life/widgets/custom_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'google_places_api_demo.dart';

class ShareYourKnowledge extends StatefulWidget {
  @override
  _ShareYourKnowledgeState createState() => _ShareYourKnowledgeState();
}

class _ShareYourKnowledgeState extends State<ShareYourKnowledge> {
  //final databaseReference = FirebaseDatabase.instance.reference().child('AdminRef');
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllPost');
  FirebaseStorage storage = FirebaseStorage.instance;

  TextEditingController titleController;
  TextEditingController descriptionController;
  TextEditingController contactNoController;
  TextEditingController websiteController;
  TextEditingController locationController;
  TextEditingController familyFriendlyFeaturesController;

  File _image;
  String _chosenValue;
  int no;

  File _image1,
      _image2,
      _image3,
      _image4,
      _image5,
      _image6,
      _image7,
      _image8,
      _image9,
      _image10;

  String location, website, phoneNo;
  String url, postUrl;
  int imageNo;
  String adminKeyValue;

  List<PostImage> imagesList = [];

  //check boxes
  bool childFriendlyValue;
  bool parkingValue;
  bool babyChangeFacility;
  bool parentsRoom;

  @override
  void initState() {
    super.initState();

    childFriendlyValue = false;
    parkingValue = false;
    babyChangeFacility = false;
    parentsRoom = false;

    titleController = TextEditingController();
    descriptionController = TextEditingController();
    contactNoController = TextEditingController();
    websiteController = TextEditingController();
    familyFriendlyFeaturesController = TextEditingController();

    no = 0;
    imageNo = 0;
    _chosenValue = eventsList[0];
    location = '';

    _permissionRequest();

    //getAdminUid();

    print('share your knowledge init: ${Constants.adminKey}');

    displayBannerImage();
  }

  @override
  Widget build(BuildContext context) {
    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera(0);
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Share your knowledge',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                /*Center(
                  child: _image == null
                      ? Image.network(
                          'https://i.ibb.co/z5g98qc/water-wallpaper.jpg')
                      : Image.file(_image),
                ),
                */

                Align(
                  alignment: Alignment.center,
                  child: url == null
                      ? Container(
                        width: 50,
                        height: 50,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          ),
                        )
                      : CachedNetworkImage(
                          imageUrl: url,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Image.asset('assets/images/placeholder.png'),
                          width: double.infinity,
                          height: 120,
                          fit: BoxFit.cover,
                        ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com'
                      ? true
                      : false,
                  child: Center(
                    child: FlatButton(
                      color: Color(0xFF0E82C6),
                      textColor: Colors.white,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        no = 0;

                        showDialog(
                            context: context,
                            builder: (BuildContext context) =>
                                imagePickerDialog);
                      },
                      child: Text(
                        'Change Photo',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    'Support your village by sharing information on services or events in your area',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),
                Container(
                  width: 340.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.grey,
                      width: 2.0,
                    ),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton(
                        value: _chosenValue,
                        items: eventsList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        onChanged: (String value) {
                          setState(() {
                            _chosenValue = value;
                          });
                        },
                        style: Theme.of(context).textTheme.title,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Title',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                CustomTextField(
                  hintText: '',
                  controller: titleController,
                  inputType: TextInputType.text,
                  obscureText: false,
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Description',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                TextField(
                  controller: descriptionController,
                  maxLines: 6,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: '',
                    hintStyle: TextStyle(fontSize: 16),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          width: 2,
                        )),
                    filled: true,
                    contentPadding: EdgeInsets.all(16),
                    fillColor: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Photos (Optional)',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: _image1 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 1;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image1,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image2 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 2;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image2,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image3 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 3;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image3,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image4 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 4;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image4,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image5 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 5;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image5,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: _image6 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 6;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image6,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image7 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 7;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image7,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image8 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 8;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image8,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image9 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 9;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image9,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: _image10 == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                ),
                                iconSize: 56,
                                color: Colors.black45,
                                onPressed: () {
                                  no = 10;

                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          imagePickerDialog);
                                },
                              )
                            : Image.file(
                                _image10,
                                width: 60,
                                height: 60,
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Location',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(height: 4.0),
                GestureDetector(
                  onTap: () async {
                    PlacesResult placesResult = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GooglePlacesAPIDemo(),
                      ),
                    );

                    setState(() {
                      location = placesResult.address;
                      website = placesResult.website;
                      phoneNo = placesResult.phoneNo;

                      contactNoController =
                          TextEditingController(text: phoneNo);
                      //locationController = TextEditingController(text: location);
                      websiteController = TextEditingController(text: website);
                    });
                  },
                  child: Container(
                    width: double.maxFinite,
                    //height: 50.0,
                    color: Colors.transparent,
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 14.0, horizontal: 8.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.black,
                              width: 2.0,
                            ),
                            borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(8.0),
                              topRight: const Radius.circular(8.0),
                              bottomRight: const Radius.circular(8.0),
                              bottomLeft: const Radius.circular(8.0),
                            )),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              width: 8.0,
                            ),
                            Expanded(child: Text('$location')),
                          ],
                        )),
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Contact Number',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                CustomTextField(
                  hintText: '',
                  controller: contactNoController,
                  inputType: TextInputType.number,
                  obscureText: false,
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Website (optional)',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                CustomTextField(
                  hintText: '',
                  controller: websiteController,
                  inputType: TextInputType.text,
                  obscureText: false,
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  '(This post will be deleted 30 days from the event)',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 14.0,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                CheckboxListTile(
                  title: Text("Child friendly"),
                  value: childFriendlyValue,
                  onChanged: (newValue) {
                    setState(() {
                      childFriendlyValue = newValue;
                    });
                  },
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
                SizedBox(
                  height: 16.0,
                ),
                CheckboxListTile(
                  title: Text("Parking Value"),
                  value: parkingValue,
                  onChanged: (newValue) {
                    setState(() {
                      parkingValue = newValue;
                    });
                  },
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
                SizedBox(
                  height: 16.0,
                ),
                CheckboxListTile(
                  title: Text("Baby Change Facility"),
                  value: babyChangeFacility,
                  onChanged: (newValue) {
                    setState(() {
                      babyChangeFacility = newValue;
                    });
                  },
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
                SizedBox(
                  height: 16.0,
                ),
                CheckboxListTile(
                  title: Text("Parents room"),
                  value: parentsRoom,
                  onChanged: (newValue) {
                    setState(() {
                      parentsRoom = newValue;
                    });
                  },
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  'Family Friendly Features (Optional)',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                TextField(
                  controller: familyFriendlyFeaturesController,
                  maxLines: 6,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: '',
                    hintStyle: TextStyle(fontSize: 16),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(width: 2, color: Colors.black),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(
                          width: 2,
                        )),
                    filled: true,
                    contentPadding: EdgeInsets.all(16),
                    fillColor: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    CustomButton(
                        width: 120.0,
                        height: 50.0,
                        borderColor: Color(Constants.buttonColor),
                        bgColor: Color(Constants.buttonColor),
                        text: 'Post',
                        icon: Icons.send,
                        callback: () => postCallback()),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future getImageFromCamera(int no) async {
    //ImagePicker imagePicker = ImagePicker();
    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    String timestamp = new DateTime.now().microsecondsSinceEpoch.toString();

    if (image != null) {
      //getCropImage(image, no);
      await getCropImage(image);

      print('image no $no');
      if (no == 0) {
        uploadImageToFirebaseStorage(image);
      } else if (no == 1) {
        await uploadPostImageToFirebaseStorage(_image1, timestamp);
      } else if (no == 2) {
        await uploadPostImageToFirebaseStorage(_image2, timestamp);
      } else if (no == 3) {
        await uploadPostImageToFirebaseStorage(_image3, timestamp);
      } else if (no == 4) {
        await uploadPostImageToFirebaseStorage(_image4, timestamp);
      } else if (no == 5) {
        await uploadPostImageToFirebaseStorage(_image5, timestamp);
      } else if (no == 6) {
        await uploadPostImageToFirebaseStorage(_image6, timestamp);
      } else if (no == 7) {
        await uploadPostImageToFirebaseStorage(_image7, timestamp);
      } else if (no == 8) {
        await uploadPostImageToFirebaseStorage(_image8, timestamp);
      } else if (no == 9) {
        await uploadPostImageToFirebaseStorage(_image9, timestamp);
      } else if (no == 10) {
        await uploadPostImageToFirebaseStorage(_image10, timestamp);
      }
    }
  } //end getImageFromCamera()

  //Future getImageFromGallery(int no) async {
  Future getImageFromGallery() async {
    //ImagePicker imagePicker = ImagePicker();

    //var image = await imagePicker.getImage(source: ImageSource.gallery);
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    String timestamp = new DateTime.now().microsecondsSinceEpoch.toString();

    if (image != null) {
      //getCropImage(image, no);
      await getCropImage(image);

      print('image no $no');

      if (no == 0) {
        uploadImageToFirebaseStorage(image);
      } else if (no == 1) {
        await uploadPostImageToFirebaseStorage(_image1, timestamp);
      } else if (no == 2) {
        await uploadPostImageToFirebaseStorage(_image2, timestamp);
      } else if (no == 3) {
        await uploadPostImageToFirebaseStorage(_image3, timestamp);
      } else if (no == 4) {
        await uploadPostImageToFirebaseStorage(_image4, timestamp);
      } else if (no == 5) {
        await uploadPostImageToFirebaseStorage(_image5, timestamp);
      } else if (no == 6) {
        await uploadPostImageToFirebaseStorage(_image6, timestamp);
      } else if (no == 7) {
        await uploadPostImageToFirebaseStorage(_image7, timestamp);
      } else if (no == 8) {
        await uploadPostImageToFirebaseStorage(_image8, timestamp);
      } else if (no == 9) {
        await uploadPostImageToFirebaseStorage(_image9, timestamp);
      } else if (no == 10) {
        await uploadPostImageToFirebaseStorage(_image10, timestamp);
      }
    }
  } //end getImageFromGallery()

  //getCropImage(var image, int no) async {
  getCropImage(var image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      setState(() {
        //_image = image;

        //newImage = croppedFile;

        ///prev code
        //_image = croppedFile;

        if (no == 0) {
          _image = croppedFile;
        } else if (no == 1) {
          _image1 = croppedFile;
        } else if (no == 2) {
          _image2 = croppedFile;
        } else if (no == 3) {
          _image3 = croppedFile;
        } else if (no == 4) {
          _image4 = croppedFile;
        } else if (no == 5) {
          _image5 = croppedFile;
        } else if (no == 6) {
          _image6 = croppedFile;
        } else if (no == 7) {
          _image7 = croppedFile;
        } else if (no == 8) {
          _image8 = croppedFile;
        } else if (no == 9) {
          _image9 = croppedFile;
        } else if (no == 10) {
          _image10 = croppedFile;
        }
      });
    } //end if
  } //getCropImage()

  _permissionRequest() async {
    final permissionValidator = EasyPermissionValidator(
      context: context,
      appName: 'Easy Permission Validator',
    );
    var result = await permissionValidator.camera();
    if (result) {
      // Do something;
    }
  }

  uploadImageToFirebaseStorage(File image) async {
    final databaseReference =
        FirebaseDatabase.instance.reference().child('bannerImages');

    String uid = Constants.uidValue;
    print('uid got: $uid');

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference =
        storage.ref().child("bannerimages/share_your_knowledge_$uid.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    setState(() {
      url = newUrl;
    });

    databaseReference.child(uid).set({'id': uid, 'value': url});
  } //end uploadImageToFirebaseStorage()

  displayBannerImage() async {
    BannerAImage bannerAImage = await getBannerImage();
    //print('banner url: ${bannerAImage.value}');

    setState(() {
      url = bannerAImage.value;
    });
  }

  Future<BannerAImage> getBannerImage() async {
    print('admin key share ur: ${Constants.adminKey}');

    Completer<BannerAImage> completer = new Completer<BannerAImage>();

    var dbRef = FirebaseDatabase.instance.reference().child("bannerImages");

    dbRef
        .orderByKey()
        .equalTo(Constants.adminKey)
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot.value != null) {
        if (snapshot.value.isNotEmpty) {
          FirebaseDatabase.instance
              .reference()
              .child('bannerImages')
              .child(Constants.adminKey)
              .once()
              .then((DataSnapshot snapshot) {
            var bannerAImage =
                new BannerAImage.fromJson(snapshot.key, snapshot.value);
            completer.complete(bannerAImage);
          });
        } else {
          print('snapshot empty');
        }
      } else {
        print('snapshot null');
      }
    });

    return completer.future;
  }

  //post image

  uploadPostImageToFirebaseStorage(File image, String timestamp) async {
    final databaseReference =
        FirebaseDatabase.instance.reference().child('AllPost').child(timestamp);

    String uid = Constants.uidValue;
    print('uid got: $uid');

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference = storage.ref().child("images/$timestamp.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    setState(() {
      postUrl = newUrl;
    });

    imageNo++;
    String strImgNo = imageNo.toString();
    imagesList.add(new PostImage.fromCode(strImgNo, postUrl));

    //databaseReference.child('images').set({'id': uid, 'value': url});

    //original code
    //databaseReference.child('images').set({strImgNo: url});
  } //end uploadImageToFirebaseStorage()

  postCallback() async {
    String timestamp = new DateTime.now().microsecondsSinceEpoch.toString();

    print('id: ${Constants.uidValue}');

    databaseReference.child(timestamp).set({
      'category': _chosenValue,
      'contactNo': contactNoController.text,
      'date': timestamp,
      'description': descriptionController.text,
      'id': Constants.uidValue,
      'linkOfWeb': websiteController.text,
      'location': location,
      'status': Constants.userEmail == 'admin123@gmail.com'
          ? 'approved'
          : 'unapproved',
      'title': titleController.text,
      'childFriendly': childFriendlyValue,
      'parkingValue': parkingValue,
      'babyChangeFamily': babyChangeFacility,
      'parentsRoom': parentsRoom,
      'familyFriendsFeatures': familyFriendlyFeaturesController.text,
    });

    /*if(_image1 != null) {
      await uploadPostImageToFirebaseStorage(_image1, timestamp);
      _image1 = null;

    }

    if(_image2 != null) {
      await uploadPostImageToFirebaseStorage(_image2, timestamp);
      _image2 = null;
    }

    if(_image3 != null) {
      uploadPostImageToFirebaseStorage(_image3, timestamp);
      _image3 = null;
    }

    if(_image4 != null) {
      uploadPostImageToFirebaseStorage(_image4, timestamp);
      _image4 = null;
    }

    if(_image5 != null) {
      uploadPostImageToFirebaseStorage(_image5, timestamp);
      _image5 = null;
    }

    if(_image6 != null) {
      uploadPostImageToFirebaseStorage(_image6, timestamp);
      _image6 = null;
    }

    if(_image7 != null) {
      uploadPostImageToFirebaseStorage(_image7, timestamp);
      _image7 = null;
    }

    if(_image8 != null) {
      uploadPostImageToFirebaseStorage(_image8, timestamp);
      _image8 = null;
    }

    if(_image9 != null) {
      uploadPostImageToFirebaseStorage(_image9, timestamp);
      _image9 = null;
    }

    if(_image10 != null) {
      uploadPostImageToFirebaseStorage(_image10, timestamp);
      _image10 = null;
    }
    */

    print('img list size: ${imagesList.length}');

    //add data to firebase database
    for (var item in imagesList) {
      databaseReference.child(timestamp).child("images").update({
        item.id: item.image,
      });

      print('uploading images: ${item.id}');
    }
    //databaseReference.child('images').set({strImgNo: url});

    Fluttertoast.showToast(
        msg: 'Post uploaded successfully !',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 3,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}

class BannerAImage {
  final String key;
  String id;
  String value;

  BannerAImage.fromJson(this.key, Map data) {
    id = data['id'];
    if (id == null) {
      id = '';
    }

    value = data['value'];
    if (value == null) {
      value = '';
    }
  }
}
