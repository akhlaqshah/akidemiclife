import 'dart:io';
import 'package:akidemic_life/models/blog.dart';
import 'package:akidemic_life/models/post.dart';
import 'package:akidemic_life/pages/blog_webview_page.dart';
import 'package:akidemic_life/utils/constants.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:akidemic_life/utils/database_interface.dart';


class BlogPage extends StatefulWidget {
  @override
  _BlogPageState createState() => _BlogPageState();
}

class _BlogPageState extends State<BlogPage> {

  final databaseReference = FirebaseDatabase.instance.reference().child('Blogs');
  FirebaseStorage storage = FirebaseStorage.instance;

  List<Map<dynamic, dynamic>> lists = [];
  List<Blog> blogsList = [];

  TextEditingController blogTitleController;
  TextEditingController blogDescriptionController;
  TextEditingController blogUrlController;

  File _image;
  File imageToUpload;
  List<Post> posts;

  String timestamp;
  String imageUri;

  @override
  void initState() {
    super.initState();

    blogTitleController = TextEditingController();
    blogDescriptionController = TextEditingController();
    blogUrlController = TextEditingController();

    posts = DatabaseInterface.getBlogPosts();
    _permissionRequest();

    print('email: ${Constants.userEmail}');

  }

  @override
  Widget build(BuildContext context) {

    Dialog imagePickerDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      //this right here
      child: Container(
        width: 360.0,
        height: 260.0,
        color: Color(0xFFF0F0F0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  'Choose',
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromCamera();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Camera',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        getImageFromGallery();
                        Navigator.pop(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.photo,
                            size: 48.0,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(
                            'Gallery',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      color: Color(0xFFF0F0F0),
                      textColor: Colors.green,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(8.0),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Cancel".toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Blogs',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),



                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com' ? true : false,
                  child: TextField(
                    controller: blogTitleController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'blog title',
                      hintStyle: TextStyle(fontSize: 16),
                    ),
                  ),
                ),

                SizedBox(
                  height: Constants.userEmail == 'admin123@gmail.com' ? 16.0 : 0.0,
                ),


                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com' ? true : false,
                  child: TextField(
                    controller: blogDescriptionController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'blog description',
                      hintStyle: TextStyle(fontSize: 16),
                    ),
                  ),
                ),
                SizedBox(
                  height: Constants.userEmail == 'admin123@gmail.com' ? 16.0 : 0.0,
                ),


                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com' ? true : false,
                  child: TextField(
                    controller: blogUrlController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'paste blog link here',
                      hintStyle: TextStyle(fontSize: 16),
                    ),
                  ),
                ),
                SizedBox(
                  //height: 24.0,
                  height: Constants.userEmail == 'admin123@gmail.com' ? 24.0 : 0.0,
                ),

                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com' ? true : false,
                  child: Container(
                    child: _image == null
                        ? IconButton(
                            padding: EdgeInsets.all(0),
                            icon: Icon(
                              Icons.camera_alt,
                            ),
                            iconSize: 80,
                            color: Colors.black45,
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      imagePickerDialog);
                            },
                          )
                        : Image.file(
                            _image,
                            width: 60,
                            height: 60,
                            fit: BoxFit.fill,
                          ),
                  ),
                ),
                SizedBox(
                  //height: 16.0,
                  height: Constants.userEmail == 'admin123@gmail.com' ? 16.0 : 0.0,
                ),

                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com' ? true : false,
                  child: Container(
                    width: double.maxFinite,
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                          color: Colors.black,
                          width: 2,
                        ),
                      ),
                      onPressed: () async {
                        ///upload post
                        timestamp = new DateTime.now().microsecondsSinceEpoch.toString();
                        print('timestamp: $timestamp');

                        /// upload selected image to Firebase storage
                        await uploadImageToFirebaseStorage(imageToUpload);


                        /// add data to firebabase database
                        databaseReference.child(timestamp).set({

                          'blogDescripton':blogDescriptionController.text,
                          'blogLink':blogUrlController.text,
                          'blogTime':timestamp,
                          'blogTitle':blogTitleController.text,
                          'imageUri':imageUri

                        });


                        Fluttertoast.showToast(
                            msg: 'Upload successful !',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIosWeb: 3,
                            backgroundColor: Colors.green,
                            textColor: Colors.white,
                            fontSize: 16.0);

                        //lists.add();



                      },
                      color: Colors.white,
                      textColor: Colors.white,
                      child: Text(
                        "Upload".toUpperCase(),
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ),

                SizedBox(
                  //height: 16.0,
                  height: Constants.userEmail == 'admin123@gmail.com' ? 16.0 : 0.0,
                ),

                Visibility(
                  visible: Constants.userEmail == 'admin123@gmail.com' ? true : false,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: Container(
                      height: 2.0,
                      width: double.maxFinite,
                      color: Colors.white,
                    ),
                  ),
                ),

                SizedBox(
                  //height: 16.0,
                  height: Constants.userEmail == 'admin123@gmail.com' ? 16.0 : 0.0,
                ),

                ///posts listview

              FutureBuilder(
                  future: databaseReference.once(),
                  builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
                    if (snapshot.hasData) {
                      lists.clear();
                      Map<dynamic, dynamic> values = snapshot.data.value;
                      values.forEach((key, values) {
                        lists.add(values);
                      });
                      return ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: lists.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(

                              onTap: () {
                                print('post item clicked');

                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => BlogWebviewPage(imageUrl: '${lists[index]['imageUri']}',
                                      blogAddress: '${lists[index]['blogLink']}',),
                                  ),
                                );


                              },

                              child: Card(
                                elevation: 5.0,
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[

                                          Text(
                                            //'${posts[index].getTitle()}',
                                            lists[index]['blogTitle'],
                                            style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.blue,
                                            ),
                                          ),


                                          Center(
                                            child: FlatButton(
                                              color: Colors.white,
                                              textColor: Colors.white,
                                              disabledColor: Colors.grey,
                                              disabledTextColor: Colors.black,
                                              padding: EdgeInsets.all(8.0),
                                              splashColor: Colors.blueAccent,
                                              onPressed: () {

                                                ///delete post
                                                print('post item clicked');

                                                databaseReference.child(lists[index]['blogTime']).remove().then((_) {

                                                  setState(() {
                                                    lists.removeAt(index);
                                                  });

                                                });

                                              },
                                              child: Text(
                                                'delete',
                                                style: TextStyle(
                                                  fontSize: 20,
                                                  color: Colors.red,
                                                ),
                                              ),

                                            ),
                                          ),




                                        ],
                                      ),

                                      SizedBox(height: 8.0,),

                                      //Image.network('https://i.ibb.co/z5g98qc/water-wallpaper.jpg',),
                                      Image.network(lists[index]['imageUri'],),

                                      SizedBox(height: 8.0,),

                                      Text(
                                        //'${posts[index].getDescription()}',
                                        lists[index]['blogDescripton'],
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.black,
                                        ),
                                      ),



                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                    }
                    return CircularProgressIndicator();
                  }),





                /*


                ListView.builder(
                  itemCount: posts.length,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return InkWell(

                      onTap: () {


                        print('post item clicked');

                      },

                      child: Card(
                        elevation: 5.0,
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[

                                  Text(
                                    '${posts[index].getTitle()}',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.blue,
                                    ),
                                  ),


                                  Center(
                                    child: FlatButton(
                                      color: Colors.white,
                                      textColor: Colors.white,
                                      disabledColor: Colors.grey,
                                      disabledTextColor: Colors.black,
                                      padding: EdgeInsets.all(8.0),
                                      splashColor: Colors.blueAccent,
                                      onPressed: () {

                                        ///delete post
                                        print('post item clicked');

                                      },
                                      child: Text(
                                        'delete',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.red,
                                        ),
                                      ),

                                    ),
                                  ),




                                ],
                              ),

                              SizedBox(height: 8.0,),

                              Image.network('https://i.ibb.co/z5g98qc/water-wallpaper.jpg',),

                              SizedBox(height: 8.0,),

                              Text(
                                '${posts[index].getDescription()}',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),



                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),


                */

              ],
            ),
          ),
        ),
      ),
    );
  }

  Future getImageFromCamera() async {
    //ImagePicker imagePicker = ImagePicker();
    //var image = await imagePicker.getImage(source: ImageSource.camera);

    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image != null) {
      //getCropImage(image, no);
      getCropImage(image);
      imageToUpload = image;
      //uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromCamera()

  Future getImageFromGallery() async {
    //ImagePicker imagePicker = ImagePicker();
    //var image = await imagePicker.getImage(source: ImageSource.gallery);
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      //getCropImage(image, no);
      getCropImage(image);

      imageToUpload = image;
      //uploadImageToFirebaseStorage(image);
    }
  } //end getImageFromGallery()

  getCropImage(var image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      setState(() {
        _image = croppedFile;
      });
    } //end if
  } //getCropImage()

  _permissionRequest() async {
    final permissionValidator = EasyPermissionValidator(
      context: context,
      appName: 'Easy Permission Validator',
    );
    var result = await permissionValidator.camera();
    if (result) {
      // Do something;
    }
  }


  uploadImageToFirebaseStorage(File image) async {

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference = storage.ref().child("Blogimages/$timestamp.jpg");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(image);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url

    String newUrl = await taskSnapshot.ref.getDownloadURL();

    if(newUrl != null) {

      setState(()  {
        imageUri = newUrl;
      });



    }


  } //end uploadImageToFirebaseStorage()





}
