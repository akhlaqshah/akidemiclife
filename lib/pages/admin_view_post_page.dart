import 'package:akidemic_life/pages/admin_edit_post_page.dart';
import 'package:akidemic_life/utils/utils.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class AdminViewPostPage extends StatefulWidget {
  Map postData;
  AdminViewPostPage({this.postData});

  @override
  _AdminViewPostPageState createState() => _AdminViewPostPageState();
}

class _AdminViewPostPageState extends State<AdminViewPostPage> {
  final databaseReference =
      FirebaseDatabase.instance.reference().child('AllPost');

  int _current = 0;

  TextEditingController descriptionController;
  String desc;
  List imagesList = [];
  bool progressVisibility;
  bool singleImage = false;
  String imageUrl;
  bool noImageAvailable = false;
  

  @override
  void initState() {
    super.initState();

    progressVisibility = false;

    desc = 'test description';
    descriptionController =
        TextEditingController(text: widget.postData['description']);
    imagesList = widget.postData['images'];

    //print('images length: ${imagesList.length}');

    if (imagesList != null) {
      if (imagesList.length <= 1) {
        singleImage = true;

        imageUrl = imagesList[0] ?? imagesList[imagesList.length - 1];

        print('imagesUrl init : $imageUrl and $singleImage');
      }
    } else {
      noImageAvailable = true;
    }
   
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: Center(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: SafeArea(
                child: Container(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    
                      SizedBox(
                        height: 20.0,
                      ),
                      Center(
                        child: Text(
                          widget.postData['title'],
                          style: TextStyle(color: Colors.black, fontSize: 20.0),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        margin: EdgeInsets.all(16.0),
                        child: noImageAvailable
                            ? Center(
                              child: Image.network(
                              
                                imageUrl ??
                                  'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg', height: 200,),
                            )
                            : singleImage
                                ? Center(
                                  child: Image.network(imageUrl ??
                                      'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg', height: 200,),
                                )
                                : Stack(
                                    alignment: Alignment.bottomRight,
                                    children: <Widget>[
                                      CarouselSlider(
                                        viewportFraction: 1.0,
                                        aspectRatio: 2.0,
                                        autoPlay: false,
                                        initialPage: 1,
                                        reverse: true,
                                        enlargeCenterPage: false,
                                        items: map<Widget>(
                                          //imgList,
                                          imagesList,
                                          (index, i) {
                                            return Container(
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: NetworkImage(imagesList[
                                                            index] ??
                                                        'https://thumbs.dreamstime.com/z/no-image-available-icon-photo-camera-flat-vector-illustration-132483097.jpg'),
                                                    fit: BoxFit.cover),
                                              ),
                                            );
                                          },
                                        ),
                                        onPageChanged: (index) {
                                          setState(() {
                                            _current = index;
                                          });
                                        },
                                      ),
                                      indicator(_current),
                                    ],
                                  ),
                      ),
                      TextField(
                        controller: descriptionController,
                        maxLines: 6,
                        readOnly: true,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          hintText: '',
                          hintStyle: TextStyle(fontSize: 16),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide:
                                BorderSide(width: 2, color: Colors.black),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide:
                                BorderSide(width: 2, color: Colors.black),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide:
                                BorderSide(width: 2, color: Colors.black),
                          ),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide: BorderSide(
                                width: 2,
                              )),
                          filled: true,
                          contentPadding: EdgeInsets.all(16),
                          fillColor: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      InkWell(
                        onTap: () {
                          ///launch url in web browser
                          Utils.launchURL(widget.postData['linkOfWeb']);
                        },
                        child: Text(
                          'Click here to visit website',
                          style: TextStyle(
                            color: Colors.deepPurple,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            'Contact: ',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            widget.postData['contactNo'],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Text(
                        'Address: ' + widget.postData['location'],
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 42.0,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(color: Colors.red)),
                                onPressed: () {
                                  //delete post action

                                  String dt = widget.postData['date'];
                                  print('date $dt');

                                  databaseReference
                                      .child(widget.postData['date'])
                                      .remove()
                                      .then((_) {
                                    Navigator.pop(context, 1);
                                  });
                                },
                                color: Colors.red,
                                textColor: Colors.white,
                                child: Text("Delete",
                                    style: TextStyle(fontSize: 14)),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: widget.postData['status'] != 'approved'
                                ? Container(
                                    height: 42.0,
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          side: BorderSide(color: Colors.teal)),
                                      onPressed: () {
                                        print('update called');
                                        setState(() {
                                          progressVisibility = true;
                                        });

                                        databaseReference
                                            .child(widget.postData['date'])
                                            .update({
                                          'status': 'approved',
                                          'reason': null,
                                        });

                                        setState(() {
                                          progressVisibility = false;
                                        });

                                        Navigator.pop(context, 2);
                                      },
                                      color: Colors.teal,
                                      textColor: Colors.white,
                                      child: Text("Approve",
                                          style: TextStyle(fontSize: 14)),
                                    ),
                                  )
                                : Container(),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: Container(
                              height: 42.0,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(color: Colors.green)),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AdminEditPostPage(
                                        postData: widget.postData,
                                      ),
                                    ),
                                  );
                                },
                                color: Colors.green,
                                textColor: Colors.white,
                                child: Text("Edit",
                                    style: TextStyle(fontSize: 14)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: progressVisibility,
              child: CircularProgressIndicator(),
            ),
          ],
        ),
      ),
    );
  }

  deletePostAction() {
    //delete post action

    String dt = widget.postData['date'];
    print('date $dt');

    databaseReference.child(widget.postData['date']).remove().then((_) {
      Navigator.pop(context, 1);
    });
  }

  editPostAction() {
/*
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditPostPage(postData: widget.postData,),
      ),
    );*/
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  Padding indicator(int _current) {
    return Padding(
      padding: const EdgeInsets.only(right: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: map<Widget>(
          //imgList,
          imagesList,
          (index, url) {
            return Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Color.fromRGBO(0, 0, 0, 0.9)
                      : Color.fromRGBO(0, 0, 0, 0.4)),
            );
          },
        ),
      ),
    );
  }
}
