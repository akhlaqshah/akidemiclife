import 'package:akidemic_life/utils/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'home_page.dart';
import 'no_email_verification_page.dart';


class LoadingPage extends StatefulWidget {

  String name, contact;

  var newUser;

  LoadingPage({this.name, this.contact, this.newUser});

  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {


  @override
  void initState() {
    super.initState();

    print('inside loading init()');


    checkEmailVerification();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0E82C6),
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  checkEmailVerification() {

    var user = FirebaseAuth.instance.currentUser;


    if (user.emailVerified)
    {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => HomePage(title: 'AKIDemic Life',),
        ),
      );

    }
    else
    {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => NoEmailVerificationPage(name: widget.name, phoneNo: widget.contact,
              newUser: widget.newUser),
        ),
      );
    }

  }//end checkEmailVerification()



}
