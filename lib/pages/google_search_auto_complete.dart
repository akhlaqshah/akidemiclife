import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:google_place/google_place.dart';


class GoogleSearchAutoComplete extends StatefulWidget {
  @override
  _GoogleSearchAutoCompleteState createState() => _GoogleSearchAutoCompleteState();
}

class _GoogleSearchAutoCompleteState extends State<GoogleSearchAutoComplete> {

  final _controller = TextEditingController();

  String _streetNumber = '';
  String _street = '';
  String _city = '';
  String _zipCode = '';
  String _phoneNo = '';
  String _website = '';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Places Autocomplete Demo'),
      ),
      body: Column(
        children: <Widget>[

          TextField(
            controller: _controller,
            readOnly: true,
            onTap: () async {
              // placeholder for our places search later

              final sessionToken = Uuid().v4();
              final Suggestion result = await showSearch(
                context: context,
                delegate: AddressSearch(sessionToken),
              );
              // This will change the text displayed in the TextField
              if (result != null) {

                print('place id: ${result.placeId}');



                final placeDetails = await PlaceApiProvider(sessionToken)
                    //.getPlaceDetailFromId('ChIJjfiv0HYBGTkRciFD43Gj5tI');
                    .getPlaceDetailFromId(result.placeId);
                setState(() {
                  _controller.text = result.description;
                  _streetNumber = placeDetails.streetNumber;
                  _street = placeDetails.street;
                  _city = placeDetails.city;
                  _zipCode = placeDetails.zipCode;
                  _phoneNo = placeDetails.phoneNo;
                  _website = placeDetails.website;
                });


              }



            },
            decoration: InputDecoration(
              icon: Container(
                margin: EdgeInsets.only(left: 20),
                width: 10,
                height: 10,
                child: Icon(
                  Icons.home,
                  color: Colors.black,
                ),
              ),
              hintText: "Enter your shipping address",
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(left: 8.0, top: 16.0),
            ),
          ),

          SizedBox(height: 20.0),



        Text('Street Number: $_streetNumber'),
          Text('Street: $_street'),
          Text('City: $_city'),
          Text('ZIP Code: $_zipCode'),
          Text('Phone no: $_phoneNo'),
          Text('Website: $_website'),


        ],
      ),
    );
  }


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}




class AddressSearch extends SearchDelegate<Suggestion> {
  AddressSearch(this.sessionToken) {
    apiClient = PlaceApiProvider(sessionToken);
  }

  final sessionToken;
  PlaceApiProvider apiClient;

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        tooltip: 'Clear',
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return FutureBuilder(
      future: query == ""
          ? null
          : apiClient.fetchSuggestions(
          query, Localizations.localeOf(context).languageCode),
      builder: (context, snapshot) => query == ''
          ? Container(
        padding: EdgeInsets.all(16.0),
        child: Text('Enter your address'),
      )
          : snapshot.hasData
          ? ListView.builder(
        itemBuilder: (context, index) => ListTile(
          title:
          Text((snapshot.data[index] as Suggestion).description),
          onTap: () {
            close(context, snapshot.data[index] as Suggestion);
          },
        ),
        itemCount: snapshot.data.length,
      )
          : Container(child: Text('Loading...')),
    );
  }
}

// For storing our result
class Suggestion {
  final String placeId;
  final String description;

  Suggestion(this.placeId, this.description);

  @override
  String toString() {
    return 'Suggestion(description: $description, placeId: $placeId)';
  }
}


class PlaceApiProvider {
  final client = Client();

  PlaceApiProvider(this.sessionToken);

  final sessionToken;



  static final String androidKey = 'AIzaSyDmDjb3-Etcxtrti2n5hS2XfmqDFHk8RzY';
  static final String iosKey = 'AIzaSyDmDjb3-Etcxtrti2n5hS2XfmqDFHk8RzY';
  final apiKey = Platform.isAndroid ? androidKey : iosKey;

  Future<List<Suggestion>> fetchSuggestions(String input, String lang) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=address&language=$lang&components=country:pk&key=$apiKey&sessiontoken=$sessionToken';
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        // compose suggestions in a list
        return result['predictions']
            .map<Suggestion>((p) => Suggestion(p['place_id'], p['description']))
            .toList();
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return [];
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }


  Future<Place> getPlaceDetailFromId(String placeId) async {
    final request =
        //'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&fields=address_component&key=$apiKey&sessiontoken=$sessionToken';
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&fields=address_component,formatted_phone_number,website&key=$apiKey&sessiontoken=$sessionToken';
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        final components =
        result['result']['address_components'] as List<dynamic>;
        // build result
        final place = Place();
        components.forEach((c) {
          final List type = c['types'];
          if (type.contains('street_number')) {
            place.streetNumber = c['long_name'];
          }
          if (type.contains('route')) {
            place.street = c['long_name'];
          }
          if (type.contains('locality')) {
            place.city = c['long_name'];
          }
          if (type.contains('postal_code')) {
            place.zipCode = c['long_name'];
          }



        });

        place.phoneNo = result['result']['formatted_phone_number'];
        place.website = result['result']['website'];

        return place;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }


}

class Place {
  String streetNumber;
  String street;
  String city;
  String zipCode;
  String phoneNo;
  String website;

  Place({
    this.streetNumber,
    this.street,
    this.city,
    this.zipCode,
    this.phoneNo,
    this.website
  });

  @override
  String toString() {
    return 'Place(streetNumber: $streetNumber, street: $street, city: $city, zipCode: $zipCode, phoneNo: $phoneNo, website: $website)';
  }
}

