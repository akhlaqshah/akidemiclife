import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  @override
  _WebViewPageState createState() => _WebViewPageState();
}



class _WebViewPageState extends State<WebViewPage> {


bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Blogs"),),
      body:  WebView(
        initialUrl: 'https://akidemiclife.com/news',
        javascriptMode: JavascriptMode.unrestricted,
        gestureNavigationEnabled: true,
        
        
      ),
    );
  }
}