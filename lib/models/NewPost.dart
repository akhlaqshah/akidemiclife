class NewPost {

  String category;
  String contactNo;
  String date;
  String description;
  String id;
  String linkOfWeb;
  String location;
  String title;
  String familyFriendsFeatures;

  //list of images
  List<dynamic> images;

  bool childFriendly;
  bool parkingValue;
  bool babyChangeFacility;
  bool parentsRoom;

  //admin
  String status;


  //review
  String review;
  String rating;

  NewPost
      (
        this.category,
        this.contactNo,
        this.date,
        this.description,
        this.id,
        this.linkOfWeb,
        this.location,
        this.title,
        this.images,
        this.childFriendly,
        this.parkingValue,
        this.babyChangeFacility,
        this.parentsRoom,
        this.status,
        this.review,
        this.rating,
        this.familyFriendsFeatures
      );


}
