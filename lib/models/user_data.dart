/**not using this class**/
class UserData {

  String _fullName;
  String _phoneNo;
  String _instituteName;

  String _location;
  String _uid;

  UserData(this._fullName, this._phoneNo, this._instituteName, this._location,
      this._uid);

  String get uid => _uid;

  set uid(String value) {
    _uid = value;
  }

  String get location => _location;

  set location(String value) {
    _location = value;
  }

  String get instituteName => _instituteName;

  set instituteName(String value) {
    _instituteName = value;
  }

  String get phoneNo => _phoneNo;

  set phoneNo(String value) {
    _phoneNo = value;
  }

  String get fullName => _fullName;

  set fullName(String value) {
    _fullName = value;
  }


/*UserData.fromJson(this.key, Map data) {
    fullName = data['fullName'];
    if(fullName == null)
      fullName = '';


    phoneNo = data['phoneNumber'];
    if(phoneNo == null)
      phoneNo = '';

    instituteName = data['instituteName'];
    if(instituteName == null)
      instituteName = '';

    location = data['location'];
    if(location == null)
      location = '';

    uid = data['uid'];
    if(uid == null)
      uid = '';

  }*/
}