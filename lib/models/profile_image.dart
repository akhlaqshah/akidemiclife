class ProfileImage {

  final String key;
  String id;
  String value;

  ProfileImage.fromJson(this.key, Map data) {
    id = data['id'];
    if (id == null) {
      id = '';
    }

    value = data['value'];
    if(value == null) {
      value = '';
    }
  }

}