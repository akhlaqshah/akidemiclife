class Post {

  String title;
  String img;
  String description;
  String postURL;

  Post({this.title, this.img, this.description, this.postURL});

  void setTitle(String title) {

    this.title = title;
  }

  String getTitle() {
    return title;
  }

  void setImage(String img) {
    this.img = img;
  }

  String getImg() {
    return img;
  }

  void setDescription(String description) {
    this.description = description;
  }

  String getDescription() {
    return description;
  }

  void setPostURL(String postURL) {
    this.postURL = postURL;
  }

  String getPostURL() {
    return postURL;
  }



}