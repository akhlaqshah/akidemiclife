class UserAccountInfo {

  final String key;
  String fullName;
  String phoneNumber;
  String uid;


  UserAccountInfo.fromJson(this.key, Map data) {

    uid = data['userId'];
    if (uid == null) {
      uid = '';
    }

    fullName = data['fullName'];
    if (fullName == null) {
      fullName = '';
    }

    phoneNumber = data['phoneNumber'];
    if(phoneNumber == null) {
      phoneNumber = '';
    }


  }
}
