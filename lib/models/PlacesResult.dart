class PlacesResult {

  String address;
  String phoneNo;
  String website;

  PlacesResult({this.address, this.phoneNo, this.website});

  String getAddress() {
    return address;
  }

  String getPhoneNo() {
    return phoneNo;
  }

  String getWebsite() {
    return website;
  }


}