class Blog {


  final String key;
  String blogDescription;
  String blogLink;
  String blogTime;
  String imageUri;

  Blog.fromJson(this.key, Map data) {

    blogDescription = data['blogDescription'];
    if (blogDescription == null) {
      blogDescription = '';
    }

    blogLink = data['blogLink'];
    if (blogLink == null) {
      blogLink = '';
    }

    blogTime = data['blogTime'];
    if (blogTime == null) {
      blogTime = '';
    }
    imageUri = data['imageUri'];
    if (imageUri == null) {
      imageUri = '';
    }

  }

}